<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property string $id
 * @property string $user_id
 * @property string $district_id
 * @property string $title
 * @property float $luas_tanah
 * @property float $luas_bangunan
 * @property int $harga
 * @property int $listrik
 * @property string $alamat
 * @property string $lokasi
 * @property string $deskripsi
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property District $district
 * @property HouseFeatureDetail[] $houseFeatureDetails
 * @property HouseOrder[] $houseOrders
 * @property Upload[] $uploads
 */
class Listing extends Model
{
    use SoftDeletes;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id', 'user_id', 'district_id','province_id','regenci_id', 'title', 'luas_tanah', 'luas_bangunan', 'harga', 'listrik', 'alamat', 'lokasi', 'deskripsi', 'kamar_mandi', 'kamar_tidur', 'status'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\District');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houseFeatureDetails()
    {
        return $this->hasMany('App\HouseFeatureDetail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houseOrders()
    {
        return $this->hasMany('App\HouseOrder');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function uploads()
    {
        return $this->hasMany('App\Upload');
    }

    public static function getRandom($count){
        return Listing::orderByRaw("RAND()")->get()->take($count);
    }
}
