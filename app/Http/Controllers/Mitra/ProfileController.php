<?php

namespace App\Http\Controllers\Mitra;

use App\Listing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(){
        $user = Auth::user();
        return view('dashboard.mitra.profile.index', compact('user'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required'
        ]);

        if(!is_null($request->avatar)){
            if(!is_null(Auth::user()->info->avatar)){
                unlink(storage_path('app/public/' . Auth::user()->info->avatar));
            }
            $path = Storage::disk('public')->put('assets/avatar', $request->avatar);
        } else {
            $path = null;
        }


        Auth::user()->update([
            'name' => $request->name
        ]);

        Auth::user()->info->update([
            'no_hp' => $request->no_hp,
            'alamat' => $request->alamat,
            'description' => $request->description,
            'avatar' => $path
        ]);

        toastr()->success("Profil Berhasil diubah");
        return redirect()->back();
    }
}
