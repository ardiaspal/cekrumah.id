<?php

namespace App\Http\Controllers\Mitra;

use App\HouseFeatureDetail;
use App\Listing;
use App\Province;
use App\Regency;
use App\District;
use App\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.mitra.listing.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = Province::all();
        return view('dashboard.mitra.listing.create', compact('provinces'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return response()->json([
//            'asd' => 'asdasd'
//        ], 422);
        // dd($request);

        $this->validate($request, [
            'title' => 'required|min:5|max:250',
            'luas_tanah' => 'required|numeric|min:0',
            'luas_bangunan' => 'required|numeric|min:0',
            'harga' => 'required|numeric',
            'listrik' => 'required|numeric',
            'district_id' => 'required|exists:districts,id',
            'province_id' => 'required|exists:provinces,id',
            'regenci_id' => 'required|exists:regencies,id',
            'alamat' => 'required|min:5',
            'deskripsi' => 'required|min:5',
            'kamar_mandi' => 'required|numeric',
            'kamar_tidur' => 'required|numeric',
            'latitude' => 'required',
            'longitude' => 'required'
        ]);

        $request->request->set('status', 1);
        $request->request->set('id', uid('L'));
        $request->request->set('user_id', Auth::id());
        $request->request->set('lokasi', json_encode([
            'longitude' => $request->longitude,
            'latitude' => $request->latitude
        ]));
        $listing = Listing::create($request->all());
        return $listing->id;
//        toastr()->success('Properti berhasil didaftarkan, silahkan menunggu paling lama 3 hari untuk persetujuan admin');
//        return redirect()->route('mitra.listing.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        if(Auth::user()->can('update', $listing)){
            return view('dashboard.mitra.listing.show', compact('listing'));
        } else {
            return abort(403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit(Listing $listing)
    {
        if(Auth::user()->can('update', $listing)){
            $provinces = Province::all();
            $regencies = Regency::where('province_id', $listing->province_id)->get();
            $districts = District::where('regency_id', $listing->regenci_id)->get();
            return view('dashboard.mitra.listing.edit', compact('listing', 'provinces','regencies','districts'));
        } else {
            return abort(403);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listing $listing)
    {
        if(Auth::user()->can('update', $listing)){
            $this->validate($request, [
                'title' => 'required|min:5|max:250',
                'luas_tanah' => 'required|numeric|min:0',
                'luas_bangunan' => 'required|numeric|min:0',
                'harga' => 'required|numeric',
                'listrik' => 'required|numeric',
                'district_id' => 'required|exists:districts,id',
                'province_id' => 'required|exists:provinces,id',
                'regenci_id' => 'required|exists:regencies,id',
                'alamat' => 'required|min:5',
                'deskripsi' => 'required|min:5',
                'kamar_mandi' => 'required|numeric',
                'kamar_tidur' => 'required|numeric',
                'latitude' => 'required',
                'longitude' => 'required'
            ]);

            $request->request->set('status', 1);
            $request->request->set('status', $listing->status);
            $request->request->set('lokasi', json_encode([
                'longitude' => $request->longitude,
                'latitude' => $request->latitude
            ]));
            $listing->update($request->all());
            toastr()->success('Properti berhasil diubah, silahkan menunggu paling lama 3 hari untuk persetujuan admin');
            return redirect()->route('mitra.listing.index');
        } else {
            return abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listing $listing)
    {
        //
    }

    public function addFeature($id, $feature_id, $val){
        HouseFeatureDetail::create([
            'id' => uid('FTD'),
            'house_feature_id' => $feature_id,
            'listing_id' => $id,
            'value' => $val
        ]);
        return $id . "--" . $feature_id . "--" . $val;
    }

    public function uploadImage($id, Request $request){
        $listing = Listing::findOrFail($id);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = Storage::disk('public')->put('listings/' . $listing->id . '', $file);
            $thumbnail = Image::make($file)->fit(120, 120)->encode($file->getClientOriginalExtension(), 100);
            Storage::disk('public')->put('listing-thumbnail/' . $path, $thumbnail);
            $upload = Upload::create([
                'id' => uid('G'),
                'listing_id' => $listing->id,
                'thumbnail' => 'listing-thumbnail/' . $path,
                'size' => $file->getSize(),
                'path' => $path
            ]);
            return $upload;
        } else {
            return "failed";
            // echo "<span class='text-danger'>No such a file</span>";
        }
    }

    public function removeImage($id, Request $request){
        $filename = $request->filename;
        $listing = Listing::findOrFail($id);
        $uploads = Upload::where('path', $filename)->firstOrFail()->delete();
        Storage::disk('public')->delete($filename);
        return "success delete";
    }

    public function updateStatus(Request $request){
        $listing = Listing::findOrFail($request->listing);
        if(!in_array($request->status, [2,3,4,5])){
            toastr()->error("Something error");
            return redirect()->back();
        }
        $listing->update([
            'status' => $request->status
        ]);
        toastr()->success("Perubahan berhasil");
        return redirect()->back();
    }
}
