<?php

namespace App\Http\Controllers\Ajax;

use App\District;
use App\Regency;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function getKabupaten($id){
        return Regency::where('province_id', $id)->get();
    }

    public function getKota($id){
        return District::where('regency_id', $id)->get();
    }
}
