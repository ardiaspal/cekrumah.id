<?php

namespace App\Http\Controllers\Ajax;

use App\HouseFeatureDetail;
use App\HouseOrder;
use App\Listing;
use App\HouseFeature;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;

class AdminController extends Controller
{
    public function getListing()
    {
        $listings = Listing::orderBy('created_at', 'desc')
            ->get();
        foreach ($listings as $index => $listing) {
            $listing->no = $index + 1;
        }

        return DataTables::of($listings)
            ->addColumn('judul', function ($q) {
                return substr($q->title, 0, 50);
            })
            ->addColumn('provinsi', function ($q) {
                return $q->district->regency->province->name;
            })
            ->addColumn('l_tanah', function ($q) {
                return $q->luas_tanah . ' m';
            })
            ->addColumn('l_bangunan', function ($q) {
                return $q->luas_bangunan . ' m';
            })
            ->addColumn('harga_format', function ($q) {
                return price_format($q->harga);
            })
            ->addColumn('action', function ($q) {
                return \view('custom-view.admin-listing-dropdown', ['listing' => $q])->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function getOrder()
    {
        $orders = HouseOrder::get();
        foreach ($orders as $index => $order) {
            $order->no = $index + 1;
        }

        return DataTables::of($orders)
            ->addColumn('buyer', function ($q) {
                return $q->buyer->name;
            })
            ->addColumn('rumah', function ($q) {
                return substr($q->listing->title, 0, 50);
            })
            ->addColumn('harga', function ($q) {
                return price_format($q->listing->harga);
            })
            ->addColumn('status_', function ($q) {
                return $q->status == 1 ? 'Active' : 'Inactive';
            })
            ->make(true);
    }

    public function getSarana()
    {
        $sarana = HouseFeature::get();
        foreach ($sarana as $index => $s) {
            $s->no = $index + 1;
        }

        return DataTables::of($sarana)
            ->addColumn('jenis', function ($q) {
                if ($q->tipe == 1) {
                    return 'Check';
                } else if ($q->tipe == 2) {
                    return 'Number';
                } else if ($q->tipe == 3) {
                    return 'Text';
                }
            })
            ->addColumn('jumlah', function ($q) {
                return $q->houseFeatureDetails->count();
            })
            ->addColumn('delete', function ($q) {
                return '<button class="btn btn-danger" onclick="deleteData(\'' . $q->id . '\')">Hapus</button>';
            })
            ->rawColumns(['delete'])
            ->make(true);
    }

    public function getRemainHouseFeatureListing($listing_id = null)
    {
        if ($listing_id == null) {
            return HouseFeature::get();
        }
        return HouseFeature::whereDoesntHave('houseFeatureDetails', function ($q) use ($listing_id) {
            $q->where('listing_id', $listing_id);
        })->get();
    }

    public function getHouseFeatureListing($listing_id)
    {
        return HouseFeatureDetail::with('houseFeature')->where('listing_id', $listing_id)
            ->get();
    }
}
