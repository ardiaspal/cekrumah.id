<?php

namespace App\Http\Controllers\Ajax;

use App\Listing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class MitraController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:mitra');
    }

    public function getListing()
    {
        $listings = Auth::user()->listings;
        foreach ($listings as $index => $listing) {
            $listing->no = $index + 1;
        }

        return DataTables::of($listings)
            ->addColumn('judul', function ($q) {
                return substr($q->title, 0, 50);
            })
            ->addColumn('provinsi', function ($q) {
                return $q->district->regency->province->name;
            })
            ->addColumn('l_tanah', function ($q) {
                return $q->luas_tanah . ' m';
            })
            ->addColumn('l_bangunan', function ($q) {
                return $q->luas_bangunan . ' m';
            })
            ->addColumn('harga_format', function ($q) {
                return price_format($q->harga);
            })
            ->addColumn('action', function($q){
                if($q->status == 1){
                    return "Belum Disetujui";
                } else if($q->status == 2){
                    return "<button class=\"btn btn-danger\" onclick=\"action('deactivate', ". $q->id .")\">Nonaktifkan</button>";
                } else if($q->status == 3){
                    return "<button class=\"btn btn-primary\" onclick=\"action('activate', ". $q->id .")\">Aktifkan</button>";
                } else {
                    return "Diblockir Admin";
                }
            })
            ->make(true);
    }
}
