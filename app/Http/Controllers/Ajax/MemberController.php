<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class MemberController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:member');
    }

    public function getWishlist()
    {
        $wishlists = Auth::user()->houseOrders;
        foreach ($wishlists as $index => $wishlist) {
            $wishlist->no = $index + 1;
        }
        return DataTables::of($wishlists)
            ->addColumn('title', function ($q) {
                return substr($q->listing->title, 0, 60) . " ...";
            })
            ->addColumn('district', function ($q) {
                return ucwords(strtolower($q->listing->district->name));
            })
            ->addColumn('luas_bangunan', function ($q) {
                return $q->listing->luas_bangunan . " m<sup>2</sup>";
            })
            ->addColumn('harga_format', function ($q) {
                return "Rp " . number_format($q->listing->harga, 0);
            })
            ->addColumn('status', function ($q) {
                return $q->listing->status == 2 ? 'Tersedia' : 'Tidak Tersedia';
            })
            ->addColumn('action', function ($q) {
                $url = route('member.wishlist.show', $q);
                return "<button class='btn btn-primary' onclick='window.location = \"" . $url . "\"'>Detail</button>";
            })
            ->rawColumns(['luas_bangunan', 'action'])
            ->make(true);
    }
}
