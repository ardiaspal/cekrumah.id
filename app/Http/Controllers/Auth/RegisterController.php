<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegisterMail;
use App\Province;
use App\User;
use App\UserInfo;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required'],
            'role' => ['required'],
            'district_id' => ['required']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        DB::beginTransaction();
        try {
            $role = 3;
            if($data['role'] == "surveyor"){
                $role = 2;
            } else if($data['role'] == "mitra"){
                $role = 4;
            }

            $verif_token = Str::random(16);
            $user = User::create([
                'id' => uid('U'),
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'verification_token' => $verif_token,
                'role_id' => $role
            ]);

            $info = UserInfo::create([
                'id' => uid('I', 8),
                'user_id' => $user->id,
                'no_hp' => $data['phone'],
                'district_id' => $data['district_id']
            ]);
            DB::commit();

            // TODO : Email Verification
            Mail::to($user->emial)->send(new RegisterMail($user));
        } catch (\Exception $e){
            DB::rollBack();
            dd($e);
        }

        return $user;
    }

    public function showRegistrationForm()
    {
        $provinces = Province::get();
        return view('auth.register', compact('provinces'));
    }
}
