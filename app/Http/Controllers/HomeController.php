<?php

namespace App\Http\Controllers;

use App\District;
use App\Listing;
use App\ListingOrderTemp;
use App\Regency;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $districts = Regency::where('name', 'KABUPATEN JEMBER')->first()->districts;
        $listings = Listing::getRandom(6);
//        $listings = Listing::orderBy('created_at', 'desc')->take(6)->get();
        return view('home', compact('listings', 'districts'));
    }

    public function sell(Request $request){
        $districts = Regency::where('name', 'KABUPATEN JEMBER')->first()->districts;
        if(is_null($request->min_price)){
            $request->min_price = 0;
        }
        if(is_null($request->max_price)){
            $request->max_price = Listing::orderBy('harga', 'desc')->first()->harga;
        }

        $listings = Listing::where('title', 'like', '%' . $request->title . '%')
            ->whereBetween('harga', [$request->min_price, $request->max_price])
            ->orderBy('created_at', 'desc');

        if(!is_null($request->bedroom)){
            $listings = $listings->where('kamar_tidur', $request->bedroom);
        }

        if(!is_null($request->bathroom)){
            $listings = $listings->where('kamar_mandi', $request->bedroom);
        }

        $listings = $listings->paginate(10);

        $listings->appends($request->only('min_price', 'max_price', 'bedroom', 'bathroom', 'district'));

        return view('listing', compact('listings', 'districts'));
    }

    public function detailListing($id){
        $listing = Listing::findOrFail($id);
        return view('listing-detail', compact('listing'));
    }

    public function underConstruction(){
        return view('under-construction');
    }

    public function orderListingTemp(Request $request){
        $this->validate($request, [
            'listing_id' => 'required|exists:listings,id',
            'email' => 'required|email|min:3|max:200',
            'phone' => 'required'
        ]);
        ListingOrderTemp::create($request->all());
        toastr()->success("Wishlist Berhasil, silahkan tunggu maksimum 7 hari");
        return redirect('/');
    }

    public function verification($token){
        $user = User::whereVerificationToken($token)->firstOrFail();
        $user->update([
            'verification_token' => null
        ]);
        if(Auth::user()){
            if(Auth::user()->id == $user->id){
                toastr("Email berhasil diverifikasi");
                return redirect()->route('dashboard');
            } else {
                return redirect()->route('dashboard');
            }
        } else {
            toastr("Email berhasil diverifikasi");
            Auth::login($user);
            return redirect()->route('dashboard');
        }
//        $user->update([
//            'verification'
//        ])
    }

    public function getProfile($id){
        $user = User::findOrFail($id);
        $listings = Listing::where('user_id', $user->id)->paginate(10);
        return view('profile-detail', compact('user', 'listings'));
    }
}
