<?php

namespace App\Http\Controllers\Admin;

use App\HouseFeatureDetail;
use App\Listing;
use App\Province;
use App\Upload;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ListingController extends Controller
{
    public function index(){
        return view('dashboard.admin.listing');
    }

    public function create(){
        $provinces = Province::all();
        return view('dashboard.admin.listing-add', compact('provinces'));
    }

    public function detail($id){
        $listing = Listing::findOrFail($id);
        return view('dashboard.admin.listing-detail', compact('listing'));
    }

    public function edit($id){
        $listing = Listing::findOrFail($id);
        $provinces = Province::all();
        return view('dashboard.admin.listing-edit', compact('listing', 'provinces'));
    }

    public function store(Request $request){
        Listing::create([
            'id' => uid('L', 8),
            'user_id' => Auth::id(),
            'district_id' => $request->district_id,
            'title' => $request->title,
            'luas_tanah' => $request->luas_tanah,
            'luas_bangunan' => $request->luas_bangunan,
            'kamar_tidur' => $request->kamar_tidur,
            'kamar_mandi' => $request->kamar_mandi,
            'harga' => $request->harga,
            'listrik' => $request->listrik,
            'alamat' => $request->alamat,
            'lokasi' => json_encode([
                'longitude' => $request->longitude,
                'latitude' => $request->latitude
            ]),
            'deskripsi' => $request->deskripsi,
        ]);
        toastr()->success("Listing berhasil dibuat");
        return redirect()->rotue('admin.listing');
    }

    public function update($id, Request $request){
        $listing = Listing::findOrFail($id);
        $listing->update([
            'title' => $request->title,
            'luas_tanah' => $request->luas_tanah,
            'luas_bangunan' => $request->luas_bangunan,
            'harga' => $request->harga,
            'listrik' => $request->listrik,
            'alamat' => $request->alamat,
            'deskripsi' => $request->deskripsi,
            'district_id' => $request->district_id,
            'kamar_mandi' => $request->kamar_mandi,
            'kamar_tidur' => $request->kamar_tidur,
            'lokasi' => json_encode([
                'longitude' => $request->longitude,
                'latitude' => $request->latitude
            ])
        ]);
        toastr()->success('Data berhasil diubah');
        return redirect()->route('admin.listing');
    }

    public function addFeature($id, $feature_id, $val){
        HouseFeatureDetail::create([
            'id' => uid('FTD'),
            'house_feature_id' => $feature_id,
            'listing_id' => $id,
            'value' => $val
        ]);
        return $id . "--" . $feature_id . "--" . $val;
    }

    public function uploadImage($id, Request $request){
        $listing = Listing::findOrFail($id);
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $path = Storage::disk('public')->put('listings/' . $listing->id . '', $file);
            $thumbnail = Image::make($file)->fit(120, 120)->encode($file->getClientOriginalExtension(), 100);
            Storage::disk('public')->put('listing-thumbnail/' . $path, $thumbnail);
            $upload = Upload::create([
                'id' => uid('G'),
                'listing_id' => $listing->id,
                'thumbnail' => 'listing-thumbnail/' . $path,
                'size' => $file->getSize(),
                'path' => $path
            ]);
            return $upload;
        } else {
            return "failed";
            // echo "<span class='text-danger'>No such a file</span>";
        }
    }

    public function removeImage($id, Request $request){
        $filename = $request->filename;
        $listing = Listing::findOrFail($id);
        $uploads = Upload::where('path', $filename)->firstOrFail()->delete();
        Storage::disk('public')->delete($filename);
        return "success delete";
    }

    public function updateStatus(Request $request){
        $listing = Listing::findOrFail($request->listing);
        if(!in_array($request->status, [2,3,4,5])){
            toastr()->error("Something error");
            return redirect()->back();
        }
        $listing->update([
            'status' => $request->status
        ]);
        toastr()->success("Perubahan berhasil");
        return redirect()->back();
    }
}
