<?php

namespace App\Http\Controllers\Admin;

use App\HouseOrder;
use App\HouseOrderConversation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderHouseController extends Controller
{
    public function index(){
        return view('dashboard.admin.house-order');
    }

    public function detail($id){
        $messages = HouseOrderConversation::where('house_order_id', $id)->get();
        $order = HouseOrder::findOrFail($id);
        return view('dashboard.admin.house-order-detail', compact('order', 'messages'));
    }
}
