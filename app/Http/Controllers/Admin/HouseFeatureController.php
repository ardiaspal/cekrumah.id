<?php

namespace App\Http\Controllers\Admin;

use App\HouseFeature;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HouseFeatureController extends Controller
{
    public function index(){
        return view('dashboard.admin.house-feature');
    }

    public function create(){
        $types = [
            1 => 'Check',
            2 => 'Number',
            3 => 'Text',
        ];
        return view('dashboard.admin.house-feature-create', compact('types'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required|min:3|max:255',
            'type' => 'required|min:1|max:3'
        ]);

        HouseFeature::create([
            'id' => uid('SRN'),
            'name' => $request->name,
            'type' => $request->type
        ]);

        toastr()->success('Sarana Berhasi Ditambahkan');
        return redirect()->route('admin.house-feature');
    }

    public function destroy(Request $request){
        HouseFeature::findOrFail($request->id)->delete();
        toastr()->success('Sarana Berhasi Dihapus');
        return redirect()->route('admin.house-feature');
    }
}
