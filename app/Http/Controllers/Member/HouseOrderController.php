<?php

namespace App\Http\Controllers\Member;

use App\HouseOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HouseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.member.wishlist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HouseOrder $houseOrder
     * @return \Illuminate\Http\Response
     */
    public function show($houseOrder)
    {
        $wishlist = HouseOrder::findOrFail($houseOrder);
        return view('dashboard.member.wishlist.show', compact('wishlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HouseOrder $houseOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(HouseOrder $houseOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\HouseOrder $houseOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HouseOrder $houseOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HouseOrder $houseOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(HouseOrder $houseOrder)
    {
        //
    }
}
