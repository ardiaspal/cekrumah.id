<?php

namespace App\Policies;

use App\User;
use App\HouseOrder;
use Illuminate\Auth\Access\HandlesAuthorization;

class HouseOrderController
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can view any house orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the house order.
     *
     * @param  \App\User  $user
     * @param  \App\HouseOrder  $houseOrder
     * @return mixed
     */
    public function view(User $user, HouseOrder $houseOrder)
    {
        return $user->id == $houseOrder->buyer_id;
    }

    /**
     * Determine whether the user can create house orders.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is('member');
    }

    /**
     * Determine whether the user can update the house order.
     *
     * @param  \App\User  $user
     * @param  \App\HouseOrder  $houseOrder
     * @return mixed
     */
    public function update(User $user, HouseOrder $houseOrder)
    {
        return $user->is('member') || $user->is('admin');
    }

    /**
     * Determine whether the user can delete the house order.
     *
     * @param  \App\User  $user
     * @param  \App\HouseOrder  $houseOrder
     * @return mixed
     */
    public function delete(User $user, HouseOrder $houseOrder)
    {
        return $user->is('admin');
    }

    /**
     * Determine whether the user can restore the house order.
     *
     * @param  \App\User  $user
     * @param  \App\HouseOrder  $houseOrder
     * @return mixed
     */
    public function restore(User $user, HouseOrder $houseOrder)
    {
        return $user->is('admin');
    }

    /**
     * Determine whether the user can permanently delete the house order.
     *
     * @param  \App\User  $user
     * @param  \App\HouseOrder  $houseOrder
     * @return mixed
     */
    public function forceDelete(User $user, HouseOrder $houseOrder)
    {
        //
    }
}
