<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property string $id
 * @property int $role_id
 * @property string $name
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Role $role
 * @property HouseOrderConversation[] $houseOrderConversations
 * @property HouseOrder[] $houseOrders
 * @property Listing[] $listings
 * @property UserInfo[] $userInfos
 */
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['id', 'role_id', 'name', 'email', 'verification_token', 'password'];
    protected $dates = ['created_at', 'updated_at'];
    protected $hidden = ['password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houseOrderConversations()
    {
        return $this->hasMany('App\HouseOrderConversation', 'buyer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function houseOrders()
    {
        return $this->hasMany('App\HouseOrder', 'buyer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function listings()
    {
        return $this->hasMany('App\Listing');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function info()
    {
        return $this->hasOne('App\UserInfo');
    }

    public function is($role){
        if(is_array($role)){
            $temp = false;
            foreach($role as $r){
                if($this->role->role == $temp){
                    $temp = true;
                    break;
                }
            }
            return $temp;
        } else {
            return $this->role->role == $role;
        }
    }
}
