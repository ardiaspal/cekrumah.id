<?php

if (! function_exists('uid')) {
    function uid($prefix = "", $length = 8) {
        return $prefix . substr(md5(uniqid(mt_rand(), true)), 0, $length - strlen($prefix));
    }
}

if (! function_exists('price_format')) {
    function price_format($price) {
        return 'Rp ' . number_format($price, 0, ',', '.');
    }
}