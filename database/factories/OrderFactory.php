<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Pesanan::class, function (Faker $faker) {
    return [
        'id' => uid('O'),
        'pembeli_id' => \App\User::all()->random()->id,
        'rumah_id' => \App\Listing::all()->random()->id,
        'status' => $faker->numberBetween(0, 1)
    ];
});
