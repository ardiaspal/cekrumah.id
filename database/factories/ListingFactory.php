<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Listing::class, function (Faker $faker) {
    return [
        'id' => uid('L'),
        'user_id' => 'Uabf7dc6',
        'title' => $faker->text(200),
        'jenis' => rand(0,1),
        'luas_tanah' => $faker->randomFloat(2, 20, 50),
        'luas_bangunan' => $faker->randomFloat(2, 20, 50),
        'harga' => rand(0, 2000000000),
        'listrik' => rand(100, 1000),
        'alamat' => $faker->address,
        'lokasi' => json_encode([
            'longitude' => $faker->randomFloat(10, 0, 360),
            'latitude' => $faker->randomFloat(10, 0, 360)
        ]),
        'deskripsi' => $faker->text,
        'tipe' => rand(1, 10)
    ];
});
