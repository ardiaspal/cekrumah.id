@extends('layouts.app')

@section('title', 'Jual Rumah')

@section('css')

@endsection

@section('content')
    <section class="error-wrap">
        <div class="container">
            <div class="row justify-content-center">

                <div class="col-lg-6 col-md-10">
                    <div class="text-center">

                        <img src="{{ asset('img/under construction.png') }}" class="img-fluid" alt="" width="100%">
                        <p>Website masih dalam tahap pembangunan, silahkan tunggu lebih lanjut</p>
                        <a class="btn btn-theme" href="{{ route('home') }}">Back To Home</a>

                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection

@section('js')
    <script>

    </script>
@endsection