@extends('layouts.app')

@section('title', 'Detail Rumah')

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
@endsection

@section('content')
    <div class="single-advance-property gray">
        <div class="container-fluid p-0">
            <div class="row align-items-center">

                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="slider-for">
                        @foreach($listing->uploads as $thumbnail)
                            <a href="{{ asset('storage/' . $thumbnail->path ) }}" class="item-slick"><img
                                        src="{{ asset('storage/' . $thumbnail->path ) }}" alt="Alt" height="500px"></a>
                        @endforeach
                    </div>

                </div>

                <div class="col-lg-5 col-md-5 col-sm-12">
                    <div class="single-advance-caption">

                        <div class="property-name-info">
                            <h4 class="property-name">{{ $listing->title }}</h4>
                            {{--<p class="property-desc">2 Bedrooms, Kitchen,and bathrooms included with balcony</p>--}}
                        </div>

                        <div class="property-price-info">
                            <h4 class="property-price">{{ price_format($listing->harga) }}</h4>
                        </div>

                        <div class="property-statement">
                            <ul>
                                <li>
                                    <i class="fas fa-expand"></i>
                                    <div class="ps-trep">
                                        <span>Luas Tanah</span>
                                        <h5 class="ps-type">{{ $listing->luas_tanah }} m<sup>2</sup></h5>
                                    </div>
                                </li>
                                <li>
                                    <i class="fas fa-home"></i>
                                    <div class="ps-trep">
                                        <span>Luas Bangunan</span>
                                        <h5 class="ps-type">{{ $listing->luas_bangunan }} m<sup>2</sup></h5>
                                    </div>
                                </li>
                                <li>
                                    <i class="lni-bolt"></i>
                                    <div class="ps-trep">
                                        <span>Daya Listrik</span>
                                        <h5 class="ps-type">{{ $listing->listrik }} Watt</h5>
                                    </div>
                                </li>
                                <li>
                                    <i class="lni-money-location"></i>
                                    <div class="ps-trep">
                                        <span>Lokasi</span>
                                        <h5 class="ps-type">{{ ucwords(strtolower($listing->district->name)) }}</h5>
                                    </div>
                                </li>
                                <li>
                                    <i class="fas fa-bath"></i>
                                    <div class="ps-trep">
                                        <span>Kamar Mandi</span>
                                        <h5 class="ps-type">{{ $listing->kamar_mandi }}</h5>
                                    </div>
                                </li>
                                <li>
                                    <i class="fas fa-bed"></i>
                                    <div class="ps-trep">
                                        <span>Kamar Tidur</span>
                                        <h5 class="ps-type">{{ $listing->kamar_tidur }}</h5>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="slider-nav">
                        @foreach($listing->uploads as $thumbnail)
                            <div class="item-slick"><img src="{{ asset('storage/' . $thumbnail->path ) }}" alt="Alt"
                                                         height="200px"></div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <section class="spd-wrap">
        <div class="container">
            <div class="row">

                <div class="col-lg-12 col-md-12">

                    <div class="slide-property-detail">

                        <div class="slide-property-first">
                            <div class="pr-price-into">
                                <h2>{{ price_format($listing->harga) }}</h2>
                                <span><i class="lni-map-marker"></i> {{ $listing->alamat }}</span>
                            </div>
                        </div>

                        <div class="slide-property-sec">
                            <div class="pr-all-info">
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="gray">
        <div class="container">
            <div class="row">

                <!-- property main detail -->
                <div class="col-lg-8 col-md-12 col-sm-12">

                    <div class="block-wrap">

                        <div class="block-header">
                            <h4 class="block-title">Info Properti</h4>
                        </div>

                        <div class="block-body">
                            <ul class="dw-proprty-info">
                                @foreach($listing->houseFeatureDetails as $detail)
                                    @if($detail->houseFeature->type != 3)
                                        @continue
                                    @endif
                                    <li><strong>{{ $detail->houseFeature->name }}</strong>{{ $detail->value }}</li>
                                @endforeach
                            </ul>
                        </div>

                    </div>

                    <div class="block-wrap">

                        <div class="block-header">
                            <h4 class="block-title">Deskripsi</h4>
                        </div>

                        <div class="block-body">
                            {!! $listing->deskripsi !!}
                        </div>

                    </div>

                    <!-- Single Block Wrap -->
                    <div class="block-wrap">

                        <div class="block-header">
                            <h4 class="block-title">Ameneties</h4>
                        </div>

                        <div class="block-body">
                            <ul class="avl-features third">
                                @foreach($listing->houseFeatureDetails as $detail)
                                    @if($detail->houseFeature->type != 1)
                                        @continue
                                    @endif
                                    <li>{{ $detail->houseFeature->name }}</li>
                                @endforeach
                            </ul>
                        </div>

                    </div>

                {{--<!-- Single Block Wrap -->--}}
                {{--<div class="block-wrap">--}}

                {{--<div class="block-header">--}}
                {{--<h4 class="block-title">Floor Plan</h4>--}}
                {{--</div>--}}

                {{--<div class="block-body">--}}
                {{--<div class="accordion" id="floor-option">--}}
                {{--<div class="card">--}}
                {{--<div class="card-header" id="firstFloor">--}}
                {{--<h2 class="mb-0">--}}
                {{--<button type="button" class="btn btn-link" data-toggle="collapse"--}}
                {{--data-target="#firstfloor">First Floor<span>740 sq ft</span></button>--}}
                {{--</h2>--}}
                {{--</div>--}}
                {{--<div id="firstfloor" class="collapse" aria-labelledby="firstFloor"--}}
                {{--data-parent="#floor-option">--}}
                {{--<div class="card-body">--}}
                {{--<img src="/img/floor.jpg" class="img-fluid" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="card">--}}
                {{--<div class="card-header" id="seconfFloor">--}}
                {{--<h2 class="mb-0">--}}
                {{--<button type="button" class="btn btn-link collapsed" data-toggle="collapse"--}}
                {{--data-target="#secondfloor">Second Floor<span>710 sq ft</span>--}}
                {{--</button>--}}
                {{--</h2>--}}
                {{--</div>--}}
                {{--<div id="secondfloor" class="collapse" aria-labelledby="seconfFloor"--}}
                {{--data-parent="#floor-option">--}}
                {{--<div class="card-body">--}}
                {{--<img src="/img/floor.jpg" class="img-fluid" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="card">--}}
                {{--<div class="card-header" id="third-garage">--}}
                {{--<h2 class="mb-0">--}}
                {{--<button type="button" class="btn btn-link collapsed" data-toggle="collapse"--}}
                {{--data-target="#garages">Garage<span>520 sq ft</span></button>--}}
                {{--</h2>--}}
                {{--</div>--}}
                {{--<div id="garages" class="collapse" aria-labelledby="third-garage"--}}
                {{--data-parent="#floor-option">--}}
                {{--<div class="card-body">--}}
                {{--<img src="/img/floor.jpg" class="img-fluid" alt=""/>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}

                {{--</div>--}}

                    <div class="block-wrap">

                        <div class="block-header">
                            <h4 class="block-title">Location</h4>
                        </div>

                        <div class="block-body">
                            <div id="map" style="height: 400px;"></div>

                        </div>

                    </div>


                </div>

                <!-- property Sidebar -->
                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="page-sidebar">

                        <!-- Agent Detail -->
                        <div class="agent-widget">
                            <form action="{{ route('order-listing') }}" method="post">
                                @csrf
                                <input type="hidden" name="listing_id" value="{{ $listing->id }}">
                                <div class="agent-title">
                                    <h4>Silahkan Isi Form Jika Anda Tertarik</h4>
                                    {{--<div class="agent-photo"><img src="/img/user-6.jpg" alt=""></div>--}}
                                    {{--<div class="agent-details">--}}
                                    {{--<h4><a href="single-property-2.html#">Shivangi Preet</a></h4>--}}
                                    {{--<span><i class="lni-phone-handset"></i>(91) 123 456 7895</span>--}}
                                    {{--</div>--}}
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email" name="email">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nomor HP" name="phone">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Pesan">Saya tertarik dengan rumah ini</textarea>
                                </div>
                                <button class="btn btn-theme full-width">Submit</button>
                            </form>
                        </div>

                        @if(!$listing->user->is('admin'))
                            <div class="agent-widget">
                                <div class="agent-title">
                                    <div class="agent-photo"><img src="{{ is_null($listing->user->info->avatar) ? asset('img/user-6.jpg') : asset('storage/' . $user->info->avatar) }}" alt=""></div>
                                    <div class="agent-details">
                                        <h4><a href="{{ route('detail-profile', $listing->user) }}">{{ $listing->user->name }}</a></h4>
                                        <span><i class="lni-phone-handset"></i>{{ $listing->user->info->no_hp }}</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        @endif

                        <div class="sidebar-widgets">

                            <h4>Lihat Rumah Lainnya</h4>

                            <div class="sidebar-property-slide">
                                @foreach(\App\Listing::getRandom(3) as $listing)
                                    <div class="single-items">
                                        <div class="property-listing property-1">

                                            <div class="listing-img-wrapper">
                                                <a href="{{ route('detail-listing', $listing) }}">
                                                    <img src="{{ asset('storage/' . $listing->uploads->first()->path) }}" class="img-fluid mx-auto" alt=""/>
                                                </a>
                                                <span class="property-type">Dijual</span>
                                            </div>

                                            <div class="listing-content">

                                                <div class="listing-detail-wrapper">
                                                    <div class="listing-short-detail">
                                                        <h4 class="listing-name"><a href="{{ route('detail-listing', $listing) }}">{{ $listing->title }}</a></h4>
                                                        <span class="listing-location"><i class="ti-location-pin"></i>{{ $listing->alamat }}</span>
                                                    </div>
                                                    {{--<div class="list-author">--}}
                                                        {{--<a href="{{ route('detail-listing', $listing) }}"><img src="/img/add-user.png"--}}
                                                                                               {{--class="img-fluid img-circle avater-30"--}}
                                                                                               {{--alt=""></a>--}}
                                                    {{--</div>--}}
                                                </div>

                                                <div class="listing-features-info">
                                                    <ul>
                                                        <li><strong>Kamar :</strong>{{ $listing->kamar_tidur }}</li>
                                                        <li><strong>Listrik :</strong>{{ $listing->listrik }}W</li>
                                                        <li><strong>Luas:</strong>{{ $listing->luas_tanah }} m<sup>2</sup></li>
                                                    </ul>
                                                </div>

                                                <div class="listing-footer-wrapper">
                                                    <div class="listing-price">
                                                        <h4 class="list-pr">Rp {{ number_format($listing->harga, 0) }}</h4>
                                                    </div>
                                                    <div class="listing-detail-btn">
                                                        <a href="{{ route('detail-listing', $listing) }}" class="more-btn">Detail</a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin=""></script>
    <script>
        $(document).ready(function () {
            var curLocation = [0, 0];
            if (curLocation[0] == 0 && curLocation[1] == 0) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    // var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                    var latlng = JSON.parse('{!! $listing->lokasi !!}');
                    curLocation = [latlng['latitude'], latlng['longitude']];
                    $('#latitude').val(latlng['latitude']);
                    $('#longitude').val(latlng['longitude']);
                    var map = L.map('map').setView(curLocation, 14);

                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: ''
                    }).addTo(map);

                    map.attributionControl.setPrefix(false);

                    var marker = new L.marker(curLocation);
                    map.addLayer(marker);
                });
            }
        });
    </script>
@endsection