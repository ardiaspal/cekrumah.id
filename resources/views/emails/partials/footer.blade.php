<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#00122c">
    <tr>
        <td style="padding: 50px;" class="p30-20">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="padding-bottom: 30px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th class="column" width="170" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="img m-center" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="images/footer_logo.jpg" width="104" height="20" border="0" alt="" /></td>
                                        </tr>
                                    </table>
                                </th>
                                <th style="padding-bottom: 25px !important; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;" class="column" width="1"></th>
                                <th class="column" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="right">
                                                <table class="center" border="0" cellspacing="0" cellpadding="0" style="text-align:center;">
                                                    <tr>
                                                        <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="images/ico_facebook.jpg" width="33" height="33" border="0" alt="" /></td>
                                                        <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="images/ico_twitter.jpg" width="33" height="33" border="0" alt="" /></td>
                                                        <td class="img" width="55" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="images/ico_instagram.jpg" width="33" height="33" border="0" alt="" /></td>
                                                        <td class="img" width="34" style="font-size:0pt; line-height:0pt; text-align:left;"><img src="images/ico_linkedin.jpg" width="33" height="33" border="0" alt="" /></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <th class="column-top" width="370" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="text-footer m-center" style="color:#666666; font-family:'Lato', Arial, sans-serif; font-size:12px; line-height:26px; text-align:left; min-width:auto !important;">Cekrumah.id <br />Jl. Sriwijaya, Sumbersari, Jember, Jawa Timur, Indonesia</td>
                                        </tr>
                                    </table>
                                </th>
                                <th style="padding-bottom: 25px !important; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;" class="column" width="1"></th>
                                <th class="column-bottom" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:bottom;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td class="text-footer right m-center" style="color:#666666; font-family:'Lato', Arial, sans-serif; font-size:12px; line-height:26px; min-width:auto !important; text-align:right;"></td>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>