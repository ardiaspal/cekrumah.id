<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="format-detection" content="date=no"/>
    <meta name="format-detection" content="address=no"/>
    <meta name="format-detection" content="telephone=no"/>
    <meta name="x-apple-disable-message-reformatting"/>
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i" rel="stylesheet"/>
    <!--<![endif]-->
    <title>Email Template</title>
    <!--[if gte mso 9]>
    <style type="text/css" media="all">
        sup {
            font-size: 100% !important;
        }
    </style>
    <![endif]-->


    <style type="text/css" media="screen">
        /* Linked Styles */
        body {
            padding: 0 !important;
            margin: 0 !important;
            display: block !important;
            min-width: 100% !important;
            width: 100% !important;
            background: #0a2641;
            -webkit-text-size-adjust: none
        }

        a {
            color: #8fc55b;
            text-decoration: none
        }

        p {
            padding: 0 !important;
            margin: 0 !important
        }

        img {
            -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */
        }

        .mcnPreviewText {
            display: none !important;
        }


        /* Mobile styles */
        @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
            u + .body .gwfw {
                width: 100% !important;
                width: 100vw !important;
            }

            .m-shell {
                width: 100% !important;
                min-width: 100% !important;
            }

            .m-center {
                text-align: center !important;
            }

            .center {
                margin: 0 auto !important;
            }

            .td {
                width: 100% !important;
                min-width: 100% !important;
            }

            .h2 {
                font-size: 35px !important;
                line-height: 40px !important;
            }

            .nav {
                font-size: 12px !important;
                line-height: 22px !important;
                padding: 10px !important;
            }

            .m-br-15 {
                height: 15px !important;
            }

            .p0-15-30 {
                padding: 0px 15px 30px 15px !important;
            }

            .p0-20-30 {
                padding: 0px 20px 30px 20px !important;
            }

            .p30-0 {
                padding: 30px 0px !important;
            }

            .p30-20 {
                padding: 30px 20px !important;
            }

            .pb30 {
                padding-bottom: 30px !important;
            }

            .p10 {
                padding: 10px !important;
            }

            .m-td,
            .m-hide {
                display: none !important;
                width: 0 !important;
                height: 0 !important;
                font-size: 0 !important;
                line-height: 0 !important;
                min-height: 0 !important;
            }

            .m-block {
                display: block !important;
            }

            .fluid-img img {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
            }

            .column,
            .column-dir,
            .column-top,
            .column-bottom,
            .column-dir-top {
                float: left !important;
                width: 100% !important;
                display: block !important;
            }

            .content-spacing {
                width: 15px !important;
            }
        }
    </style>
</head>
<body class="body"
      style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#0a2641; -webkit-text-size-adjust:none;">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f7f7f7" class="gwfw">
    <tr>
        <td align="center" valign="top" style="padding: 50px 10px;" class="p10">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="m-shell">
                <tr>
                    <td class="td" bgcolor="#ffffff"
                        style="border-radius: 12px; width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal;">
                        <!-- Main -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                <!-- Title + Copy - center -->
                                @include('emails.partials.header')
                                <!-- END Title + Copy - center -->
                                    <!-- Article -->
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tr>
                                            <td style="padding: 50px 50px 0 50px;" class="p30-20">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="h2 white center"
                                                            style="padding-bottom: 25px; font-family:'Lato', Arial, sans-serif; font-size:24px; line-height:30px; text-transform:uppercase; color:#848b95; text-align:center;">
                                                            <b>@yield('title')</b></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding: 0 50px 50px 50px;" class="p30-20">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td class="text2 grey center"
                                                            style="padding-bottom: 25px; font-family:'Lato', Arial, sans-serif; font-size:15px; line-height:28px; min-width:auto !important; color:#848b95; text-align:center;">@yield('content')</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center">
                                                            <!-- Button -->
                                                            @isset($button)
                                                                <table class="center" border="0" cellspacing="0"
                                                                       cellpadding="0" style="text-align:center;">
                                                                    <tr>
                                                                        <td class="text-button light-blue-bg"
                                                                            style="color:#ffffff; font-family:'Lato', Arial, sans-serif; font-size:16px; line-height:20px; text-align:center; text-transform:uppercase; font-weight:bold; min-width:auto !important; padding:12px 22px; border-radius:4px; background:#6cf988;">
                                                                            <a href="{{ $button["url"] }}" target="_blank"
                                                                               class="link-white"
                                                                               style="color:#ffffff; text-decoration:none;">
                                                                                <span class="link-white" style="color:#fefff6; text-decoration:none;">{{ $button["title"] }}</span>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                        @endisset
                                                        <!-- END Button -->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- END Article -->
                                    <!-- Footer -->
                                @include('emails.partials.footer')
                                <!-- END Footer -->
                                </td>
                            </tr>
                        </table>
                        <!-- END Main -->
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
