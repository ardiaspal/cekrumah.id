@extends('layouts.auth')

@section('title', 'Register')

@section('css')
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/selectric.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                    <div class="login-brand">
                        <img src="{{ asset('img/logo-min.png') }}" alt="logo" width="50%">
                    </div>
                    <div class="card card-primary">
                        <div class="card-header"><h4>Register</h4></div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="name">Nama</label>
                                        <input id="name" type="text" class="form-control" name="name" autofocus required value="{{ old('name') }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control" name="email" required value="{{ old('email') }}">
                                    <div class="invalid-feedback">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-6">
                                        <label for="password" class="d-block">Password</label>
                                        <input id="password" type="password" class="form-control pwstrength" data-indicator="pwindicator" name="password" required>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="password_confirmation" class="d-block">Password Confirmation</label>
                                        <input id="password_confirmation" type="password" class="form-control" name="password_confirmation">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-6">
                                        <label>Sebagai</label>
                                        <select class="form-control selectric" name="role" required>
                                            {{--<option value="member">Member</option>--}}
                                            <option value="surveyor">Surveyor</option>
                                            <option value="mitra">Mitra</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for="phone" class="d-block">No HP</label>
                                        <input id="phone" type="phone" class="form-control" name="phone" required value="{{ old('phone') }}">
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Provinsi</label>
                                        <select class="form-control selectric" name="province_id" required id="province">
                                            @foreach($provinces as $province)
                                                <option value="{{ $province->id }}">{{ ucfirst(strtolower($province->name)) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Kabupaten</label>
                                        <select class="form-control selectric" name="regency_id" required id="regency">

                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Kecamatan</label>
                                        <select class="form-control selectric" name="district_id" required id="district">

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="agree" class="custom-control-input" id="agree" required>
                                        <label class="custom-control-label" for="agree">I agree with the terms and conditions</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        Register
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; Cekrumah.id 2019
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('dashboard-assets/modules/js/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/jquery.selectric.min.js') }}"></script>
    <script>
        $('#province').on('change', () => {
            $('#regency')
                .find('option')
                .remove()
                .end();
            $.ajax({
                url: '{{ url('/ajax/kabupaten') }}/' + $('#province').val(),
                success: (response) => {
                    $.each(response, (key, val) => {
                        $('#regency')
                            .append($("<option></option>")
                                .attr("value", val["id"])
                                .text(toTitleCase(val["name"])));
                    });

                    $("#regency").selectric().change();
                }
            });
        }).change();

        $('#regency').on('change', () => {
            $('#district')
                .find('option')
                .remove()
                .end();
            $.ajax({
                url: '{{ url('/ajax/kecamatan') }}/' + $('#regency').val(),
                success: (response) => {
                    $.each(response, (key, val) => {
                        $('#district')
                            .append($("<option></option>")
                                .attr("value", val["id"])
                                .text(toTitleCase(val["name"])));
                    });

                    $("#district").selectric();
                }
            });
        }).change();

        function toTitleCase(str) {
            return str.replace(/\w\S*/g, function(txt){
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        }
    </script>
@endsection