@extends('layouts.app')

@section('title', 'Jual Rumah')

@section('css')

@endsection

@section('content')
    <div class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">

                    <h2 class="ipt-title">Daftar Rumah</h2>
                    <span class="ipn-subtitle">Daftar Rumah yang dijual</span>

                </div>
            </div>
        </div>
    </div>
    <!-- ============================ Page Title End ================================== -->

    <!-- ============================ All Property ================================== -->
    <section>

        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-12 list-layout">
                    <div class="row">

                        <div class="col-lg-12 col-md-12">
                            <div class="filter-fl">
                                <h4>Total Rumah yang dijual ada : <span class="theme-cl">{{ $listings->total() }}</span>
                                </h4>
                                <div class="btn-group custom-drop">
                                    {{--<button type="button" class="btn btn-order-by-filt" data-toggle="dropdown"--}}
                                            {{--aria-haspopup="true" aria-expanded="false">--}}
                                        {{--Sort By<i class="ti-angle-down"></i>--}}
                                    {{--</button>--}}
                                    <div class="dropdown-menu pull-right animated flipInX">
                                        {{--<a href="list-layout-with-sidebar.html#">Latest</a>--}}
                                        {{--<a href="list-layout-with-sidebar.html#">Most View</a>--}}
                                        {{--<a href="list-layout-with-sidebar.html#">Most Popular</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        @foreach($listings as $listing)
                            <div class="col-lg-12 col-md-12">
                                <div class="property-listing property-1">

                                    <div class="listing-img-wrapper">
                                        <a href="{{ route('detail-listing', $listing) }}">
                                            <img src="{{ asset('storage/' . $listing->uploads->first()->path) }}"
                                                 class="img-fluid mx-auto" alt=""/>
                                        </a>
                                        {{--<div class="listing-like-top">--}}
                                        {{--<i class="ti-heart"></i>--}}
                                        {{--</div>--}}
                                        {{--<div class="listing-rating">--}}
                                        {{--<i class="ti-star filled"></i>--}}
                                        {{--<i class="ti-star filled"></i>--}}
                                        {{--<i class="ti-star filled"></i>--}}
                                        {{--<i class="ti-star filled"></i>--}}
                                        {{--<i class="ti-star"></i>--}}
                                        {{--</div>--}}
                                        {{--<span class="property-type">For Sale</span>--}}
                                    </div>

                                    <div class="listing-content">

                                        <div class="listing-detail-wrapper">
                                            <div class="listing-short-detail">
                                                <h4 class="listing-name"><a
                                                            href="{{ route('detail-listing', $listing) }}">{{ substr($listing->title, 0, 50) }}</a>
                                                </h4>
                                                <span class="listing-location"><i class="ti-location-pin"></i>{{ $listing->alamat }}</span>
                                            </div>
                                            <div class="list-author">
                                                {{--<a href="list-layout-with-sidebar.html#"><img src="assets/img/add-user.png" class="img-fluid img-circle avater-30" alt=""></a>--}}
                                            </div>
                                        </div>

                                        <div class="listing-features-info">
                                            <ul>
                                                <li><strong>Luas :</strong>{{ $listing->luas_tanah }} m2</li>
                                                {{--<li><strong>Luas Bangunan :</strong>{{ $listing->luas_bangunan }} m2</li>--}}
                                                {{--<li><strong>Sqft:</strong>3,700</li>--}}
                                            </ul>
                                        </div>

                                        <div class="listing-footer-wrapper">
                                            <div class="listing-price">
                                                <h4 class="list-pr">{{ price_format($listing->harga) }}</h4>
                                            </div>
                                            <div class="listing-detail-btn">
                                                <a href="{{ route('detail-listing', $listing) }}" class="more-btn">More
                                                    Info</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>

                    <!-- Pagination -->
                    {{ $listings->links() }}
                    {{--<div class="row">--}}
                    {{--<div class="col-lg-12 col-md-12 col-sm-12">--}}
                    {{--<ul class="pagination p-center">--}}
                    {{--<li class="page-item">--}}
                    {{--<a class="page-link" href="list-layout-with-sidebar.html#" aria-label="Previous">--}}
                    {{--<span class="ti-arrow-left"></span>--}}
                    {{--<span class="sr-only">Previous</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="page-item"><a class="page-link" href="list-layout-with-sidebar.html#">1</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="list-layout-with-sidebar.html#">2</a></li>--}}
                    {{--<li class="page-item active"><a class="page-link" href="list-layout-with-sidebar.html#">3</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="list-layout-with-sidebar.html#">...</a></li>--}}
                    {{--<li class="page-item"><a class="page-link" href="list-layout-with-sidebar.html#">18</a></li>--}}
                    {{--<li class="page-item">--}}
                    {{--<a class="page-link" href="list-layout-with-sidebar.html#" aria-label="Next">--}}
                    {{--<span class="ti-arrow-right"></span>--}}
                    {{--<span class="sr-only">Next</span>--}}
                    {{--</a>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                </div>

                <div class="col-lg-4 col-md-12 col-sm-12">
                    <div class="page-sidebar">
                        <div class="sidebar-widgets">

                            <h4>Cari Rumah Baru</h4>


                            <form action="" id="search-form">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="text" class="form-control" placeholder="Masukan kata kunci"
                                               value="{{ request()->input('title') }}" name="title">
                                        <i class="ti-search"></i>
                                    </div>
                                </div>

                                {{--<div class="row">--}}
                                {{--<div class="col-lg-6 col-md-6 col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<div class="input-with-icon">--}}
                                {{--<input type="number" class="form-control" placeholder="Harga Minimum" name="min_price" value="{{ is_null(request()->min_price) || request()->min_price == 0 ? '' : request()->min_price }}">--}}
                                {{--<i class=""></i>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-6 col-md-6 col-sm-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<div class="input">--}}
                                {{--<input type="number" class="form-control" placeholder="Harga Maximum" name="max_price" value="{{ is_null(request()->max_price) || request()->max_price == 0 ? '' : request()->max_price }}">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="bedrooms" class="form-control" name="bedrooms">
                                            <option value="">&nbsp;</option>
                                            @for($i=1;$i<=5;$i++)
                                                <option value="{{ $i }}" {{ request()->get('bedrooms') == $i ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                        <i class="fas fa-bed"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="bathrooms" class="form-control" name="bathrooms">
                                            <option value="">&nbsp;</option>
                                            @for($i=1;$i<=5;$i++)
                                                <option value="{{ $i }}" {{ request()->get('bathrooms') == $i ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                        <i class="fas fa-bath"></i>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="district" name="district" class="form-control">
                                            <option value="">&nbsp;</option>
                                            @foreach($districts as $district)
                                                <option value="{{ $district->id }}" {{ request()->get('district') == $district->id ? 'selected' : '' }}>{{ $district->name }}</option>
                                            @endforeach
                                        </select>
                                        <i class="ti-briefcase"></i>
                                    </div>
                                </div>

                                <div class="range-slider">
                                    <label>Range Harga</label>
                                    <div data-min="0" data-step="100000" data-max="10000000000"
                                         data-min-name="min_price"
                                         data-max-name="max_price" data-unit="IDR" class="range-slider-ui ui-slider"
                                         aria-disabled="false"></div>
                                    <div class="clearfix"></div>
                                </div>

                                {{--<div class="range-slider">--}}
                                {{--<label>Area</label>--}}
                                {{--<div data-min="0" data-max="10000" data-min-name="min_area" data-max-name="max_area" data-unit="Sq ft" class="range-slider-ui ui-slider" aria-disabled="false"></div>--}}
                                {{--<div class="clearfix"></div>--}}
                                {{--</div>--}}

                                {{--<div class="ameneties-features">--}}
                                {{--<div class="form-group" id="module">--}}
                                {{--<a role="button" class="" data-toggle="collapse" href="list-layout-with-sidebar.html#advance-search" aria-expanded="true" aria-controls="advance-search"></a>--}}
                                {{--</div>--}}
                                {{--<div class="collapse" id="advance-search" aria-expanded="false" role="banner">--}}
                                {{--<ul class="no-ul-list">--}}
                                {{--<li>--}}
                                {{--<input id="a-1" class="checkbox-custom" name="a-1" type="checkbox">--}}
                                {{--<label for="a-1" class="checkbox-custom-label">Air Condition</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-2" class="checkbox-custom" name="a-2" type="checkbox">--}}
                                {{--<label for="a-2" class="checkbox-custom-label">Bedding</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-3" class="checkbox-custom" name="a-3" type="checkbox">--}}
                                {{--<label for="a-3" class="checkbox-custom-label">Heating</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-4" class="checkbox-custom" name="a-4" type="checkbox">--}}
                                {{--<label for="a-4" class="checkbox-custom-label">Internet</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-5" class="checkbox-custom" name="a-5" type="checkbox">--}}
                                {{--<label for="a-5" class="checkbox-custom-label">Microwave</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-6" class="checkbox-custom" name="a-6" type="checkbox">--}}
                                {{--<label for="a-6" class="checkbox-custom-label">Smoking Allow</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-7" class="checkbox-custom" name="a-7" type="checkbox">--}}
                                {{--<label for="a-7" class="checkbox-custom-label">Terrace</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-8" class="checkbox-custom" name="a-8" type="checkbox">--}}
                                {{--<label for="a-8" class="checkbox-custom-label">Balcony</label>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                {{--<input id="a-9" class="checkbox-custom" name="a-9" type="checkbox">--}}
                                {{--<label for="a-9" class="checkbox-custom-label">Icon</label>--}}
                                {{--</li>--}}
                                {{--</ul>--}}
                                {{--</div>--}}
                            </form>

                            <button class="btn btn-theme full-width" id="search-btn">Cari</button>

                            {{--</div>--}}

                        </div>
                    </div>
                    <!-- Sidebar End -->

                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $('#order-select').on('change', function () {
            console.log($(this).val());
        });

        $('input .current-min').change(function () {
            console.log($(this).text());
        });

        $('#search-btn').on('click', () => {
            var url = '{{ route('sell') }}';
            var params = $('#search-form').serialize();
            window.document.location = url + "?" + params;
        });
    </script>
@endsection