@extends('layouts.app')

@section('title', 'Home')

@section('css')

@endsection

@section('content')
    <div class="image-cover hero-banner" style="background:url(/img/city.svg) no-repeat;">
        <div class="container">
            <div class="hero-search-wrap">
                <div class="hero-search">
                    <h1>Cari Rumah Impianmu</h1>
                </div>
                <div class="hero-search-content">

                    <form id="search-form">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="text" class="form-control" placeholder="Neighborhood" name="title">
                                        <i class="ti-search"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="number" class="form-control" placeholder="Harga Minimum"
                                               name="min_price">
                                        <i class="">Rp</i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <input type="number" class="form-control" placeholder="Harga Maximum"
                                               name="max_price">
                                        <i class="">Rp</i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="bedrooms" class="form-control" name="bedroom">
                                            <option value="">&nbsp;</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <i class="fas fa-bed"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="bathrooms" class="form-control" name="bathroom">
                                            <option value="">&nbsp;</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <i class="fas fa-bath"></i>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <div class="input-with-icon">
                                        <select id="district" name="district" class="form-control">
                                            <option value="">&nbsp;</option>
                                            @foreach($districts as $district)
                                                <option value="{{ $district->id }}">{{ $district->name }}</option>
                                            @endforeach
                                        </select>
                                        <i class="ti-briefcase"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="hero-search-action">
                    <button id="search-btn" class="btn search-btn">Search Result</button>
                </div>
            </div>
        </div>
    </div>
    <section class="gray-bg">
        <div class="container">

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <h2>Lihat Rumah</h2>
                </div>
            </div>

            <div class="row">
                @foreach($listings as $listing)
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="property-listing property-2">

                            <div class="listing-img-wrapper">
                                <div class="list-img-slide">
                                    <div class="click">
                                        @foreach($listing->uploads->take(3) as $thumbnail)
                                            <div>
                                                <a href="{{ route('detail-listing', $listing) }}">
                                                    <img src="{{ asset('storage/' . $thumbnail->path) }}"
                                                         class="img-fluid mx-auto" alt=""/>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="listing-detail-wrapper pb-0">
                                <div class="listing-short-detail">
                                    <h4 class="listing-name">
                                        <a href="{{ route('detail-listing', $listing) }}">{{ $listing->title }}</a>
                                    </h4>
                                </div>
                            </div>

                            <div class="price-features-wrapper">
                                <div class="listing-price-fx">
                                    <h6 class="listing-card-info-price">{{ price_format($listing->harga) }}
                                        <span class="price-suffix"></span></h6>
                                </div>
                                <div class="list-fx-features">
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bed">{{ $listing->kamar_tidur }} Kamar Tidur</span>
                                    </div>
                                    <div class="listing-card-info-icon">
                                        <span class="inc-fleat inc-bath">{{ $listing->kamar_mandi }} Kamar Mandi</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </section>
    {{--<section>--}}
    {{--<div class="container">--}}

    {{--<div class="row">--}}
    {{--<div class="col-lg-12 col-md-12">--}}
    {{--<div class="sec-heading center">--}}
    {{--<h2>Cari Berdasarkan Kecamatan</h2>--}}
    {{--<p>Top & perfect 200+ location property.</p>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--<div class="row">--}}

    {{--<div class="col-lg-8 col-md-8">--}}
    {{--<a href="listings-list-with-sidebar.html" class="img-wrap">--}}
    {{--<div class="img-wrap-content visible">--}}
    {{--<h4>Los Angeles</h4>--}}
    {{--<span>24 Properties</span>--}}
    {{--</div>--}}
    {{--<div class="img-wrap-background" style="background-image: url(/img/tour-1.jpg);"></div>--}}
    {{--</a>--}}
    {{--</div>--}}

    {{--<div class="col-lg-4 col-md-4">--}}
    {{--<a href="listings-list-with-sidebar.html" class="img-wrap">--}}
    {{--<div class="img-wrap-content visible">--}}
    {{--<h4>Los Angeles</h4>--}}
    {{--<span>24 Properties</span>--}}
    {{--</div>--}}
    {{--<div class="img-wrap-background" style="background-image: url(/img/tour-2.jpg);"></div>--}}
    {{--</a>--}}
    {{--</div>--}}

    {{--</div>--}}

    {{--<div class="row">--}}

    {{--<div class="col-lg-4 col-md-4">--}}
    {{--<a href="listings-list-with-sidebar.html" class="img-wrap">--}}
    {{--<div class="img-wrap-content visible">--}}
    {{--<h4>Los Angeles</h4>--}}
    {{--<span>24 Properties</span>--}}
    {{--</div>--}}
    {{--<div class="img-wrap-background" style="background-image: url(/img/tour-3.jpg);"></div>--}}
    {{--</a>--}}
    {{--</div>--}}

    {{--<div class="col-lg-4 col-md-4">--}}
    {{--<a href="listings-list-with-sidebar.html" class="img-wrap">--}}
    {{--<div class="img-wrap-content visible">--}}
    {{--<h4>Los Angeles</h4>--}}
    {{--<span>24 Properties</span>--}}
    {{--</div>--}}
    {{--<div class="img-wrap-background" style="background-image: url(/img/tour-4.jpg);"></div>--}}
    {{--</a>--}}
    {{--</div>--}}

    {{--<div class="col-lg-4 col-md-4">--}}
    {{--<a href="listings-list-with-sidebar.html" class="img-wrap">--}}
    {{--<div class="img-wrap-content visible">--}}
    {{--<h4>Los Angeles</h4>--}}
    {{--<span>24 Properties</span>--}}
    {{--</div>--}}
    {{--<div class="img-wrap-background" style="background-image: url(/img/tour-5.jpg);"></div>--}}
    {{--</a>--}}
    {{--</div>--}}

    {{--</div>--}}

    {{--</div>--}}
    {{--</section>--}}
@endsection

@section('js')
    <script>
        $('#search-btn').on('click', () => {
            var url = '{{ route('sell') }}';
            var params = $('#search-form').serialize();
            window.document.location = url + "?" + params;
        });
    </script>
@endsection