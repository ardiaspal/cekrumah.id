<button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">
    Action
</button>
<div class="dropdown-menu" data-listing-id="{{ $listing->id }}">
    @if($listing->status == 1)
        <a class="dropdown-item" href="#" id="action-button" data-id="2">Publish</a>
        <a class="dropdown-item" href="#" id="block-button" data-id="4">Block</a>
    @elseif($listing->status == 2)
        <a class="dropdown-item" href="#" id="action-button" data-id="3">Nonaktifkan</a>
        <a class="dropdown-item" href="#" id="block-button" data-id="4">Block</a>
    @elseif($listing->status == 3)
        <a class="dropdown-item" href="#" id="action-button" data-id="2">Aktifkan</a>
        <a class="dropdown-item" href="#" id="block-button" data-id="4">Block</a>
    @else
        <a class="dropdown-item" href="#" id="block-button" data-id="2">Unblock</a>
    @endif
</div>

{{--@if($listing->status == 1)--}}
    {{--<button class="btn btn-primary float-right" id="action-button" data-id="2">Publish</button>--}}
    {{--<button class="btn btn-danger float-right" id="block-button" data-id="5">Block</button>--}}
{{--@elseif($listing->status == 2)--}}
    {{--<button class="btn btn-danger float-right" id="action-button" data-id="3">Nonaktifkan</button>--}}
    {{--<button class="btn btn-danger float-right" id="block-button" data-id="5">Block</button>--}}
{{--@elseif($listing->status == 3)--}}
    {{--<button class="btn btn-success float-right" id="action-button" data-id="4">Aktifkan</button>--}}
    {{--<button class="btn btn-danger float-right" id="block-button" data-id="5">Block</button>--}}
{{--@elseif($listing->status == 4)--}}
    {{--<button class="btn btn-success float-right" id="action-button" data-id="5">Unblock</button>--}}
{{--@endif--}}