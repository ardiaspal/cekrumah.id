@extends('dashboard.layouts.app')

@section('title', 'Home')

@section('css')

@endsection

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{ $wishlist->title }}</h4>
                        <small>{{ date('F, d Y | H:i:s', strtotime($wishlist->created_at)) }}</small>
                        <br>
                        <br>
                        <!--Carousel Wrapper-->
                        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                            <!--Indicators-->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                            </ol>
                            <!--/.Indicators-->
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                <!--First slide-->
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg"
                                         alt="First slide">
                                </div>
                                <!--/First slide-->
                                <!--Second slide-->
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(129).jpg"
                                         alt="Second slide">
                                </div>
                                <!--/Second slide-->
                                <!--Third slide-->
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg"
                                         alt="Third slide">
                                </div>
                                <!--/Third slide-->
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                        </div>
                        <br>
                        <hr>
                        <!--/.Carousel Wrapper-->
                        <h4>Detail</h4>
                        <br>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Jenis Listing</td>
                                <td>{{ $wishlist->jenis == 1 ? 'Dijual' : 'Disewakan' }}</td>
                            </tr>
                            <tr>
                                <td>Luas Tanah</td>
                                <td>{{ $wishlist->luas_tanah }} m</td>
                            </tr>
                            <tr>
                                <td>Luas Bangunan</td>
                                <td>{{ $wishlist->luas_bangunan }} m</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>{{ price_format($wishlist->harga) }}</td>
                            </tr>
                            <tr>
                                <td>Daya Listrik</td>
                                <td>{{ $wishlist->listrik }} W</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>{{ $wishlist->alamat }} </td>
                            </tr>
                            <tr>
                                <td>Lokasi</td>
                                <td>
                                    Longtitude : {{ json_decode($wishlist->lokasi, true)["longitude"] }} <br>
                                    Langtitude : {{ json_decode($wishlist->lokasi, true)["latitude"] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h4>Deskripsi</h4>
                        <p>{{ $wishlist->deskripsi }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>
        $('#edit-button').on('click', function(){
            window.location = '/mitra/listing/{{ $wishlist->id }}/edit';
        });
    </script>
@endsection