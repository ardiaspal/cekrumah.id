@extends('dashboard.layouts.app')

@section('title', 'Listing')

@section('css')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Wishlist Rumah</h1>
            <div class="section-header-breadcrumb">
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Wishlist</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="listing-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Judul</th>
                                        <th>Daerah</th>
                                        <th>Luas Bangunan</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- JS Libraies -->
    <script src="{{ asset('dashboard-assets/modules/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/select.bootstrap4.min.js') }}"></script>

    <script>
        $('#listing-table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('ajax.member.get-wishlist') }}",
            columns: [
                {data: 'no', name: 'no'},
                {data: 'title', name: 'title'},
                {data: 'district', name: 'district'},
                {data: 'luas_bangunan', name: 'luas_bangunan'},
                {data: 'harga_format', name: 'harga_format'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'},
            ]
        });
    </script>
@endsection