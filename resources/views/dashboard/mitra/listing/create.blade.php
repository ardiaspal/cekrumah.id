@extends('dashboard.layouts.app')

@section('title', 'Home')

@section('css-library')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
crossorigin=""/>

<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select2.min.css') }}">
@endsection

@section('css')
<link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/selectric.css') }}">
<style>
    .lds-hourglass {
        display: inline-block;
        position: relative;
        width: 64px;
        height: 64px;
    }

    .lds-hourglass:after {
        content: " ";
        display: block;
        border-radius: 50%;
        width: 0;
        height: 0;
        margin: 6px;
        box-sizing: border-box;
        border: 26px solid #6777ef;
        border-color: #6777ef transparent #6777ef transparent;
        animation: lds-hourglass 1.2s infinite;
    }

    @keyframes lds-hourglass {
        0% {
            transform: rotate(0);
            animation-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
        }
        50% {
            transform: rotate(900deg);
            animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
        }
        100% {
            transform: rotate(1800deg);
        }
    }

</style>
@endsection

@section('content')
<div class="main-content-inner">
    <div class="row">
        <div class="col-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h4>Tambah Properti</h4>
                    <br>
                    <form action="{{ route('mitra.listing.store') }}" method="post" id="form__"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="title">Judul</label>
                        <input type="text" class="form-control" id="title" name="title" required
                        value="">
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="luas-tanah">Luas Tanah</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="luas_tanah" id="luas-tanah" required
                                value="">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="luas-tanah">m²</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="luas-bangunan">Luas Bangunan</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="luas_bangunan" id="luas-bangunan"
                                required
                                value="">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="luas-bangunan">m²</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-4">
                            <label for="listrik">Daya Listrik</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="listrik" id="listrik" required
                                value="">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="listrik">Watt</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="harga">Harga</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Rp</span>
                            </div>
                            <input type="number" id="harga" name="harga" class="form-control" required
                            aria-describedby="basic-addon1" value="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label class="col-form-label">Provinsi</label>
                            <select name="province_id" id="province" class="form-control selectric" required>
                                @foreach($provinces as $province)
                                <option value="{{ $province->id }}">{{ ucwords(strtolower($province->name)) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label class="col-form-label">Kabupaten</label>
                            <select name="regenci_id" id="regency" class="form-control selectric" required>

                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label class="col-form-label">Kecamatan</label>
                            <select name="district_id" id="district" class="form-control selectric" required>

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" id="alamat" name="alamat" required
                        value="">
                    </div>

                    <div class="form-group">
                        <label for="deskripsi">Deskripsi</label>
                        <textarea class="form-control" aria-label="With textarea" name="deskripsi" required style="height: 200px"
                        id="deskripsi" rows="10"></textarea>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="kamar-mandi">Kamar Mandi</label>
                            <input type="number" class="form-control" id="kamar-mandi" name="kamar_mandi" required min="1"
                            value="">
                        </div>

                        <div class="form-group col-6">
                            <label for="kamar-tidur">Kamar Tidur</label>
                            <input type="number" class="form-control" id="kamar-tidur" name="kamar_tidur" required min="1"
                            value="">
                        </div>
                    </div>

                    {{--<div class="form-group">--}}
                        {{--<label for="map">Sarana</label>--}}
                        {{--<div class="col-md-12">--}}
                            {{--<table class="table-bordered table" id="feature-table">--}}
                                {{--<tbody>--}}
                                    {{--<tr></tr>--}}
                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-12">--}}
                            {{--<div class="row">--}}
                                {{--<div class="col-md-5">--}}
                                    {{--<select id="feature-id" class="form-control">--}}

                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-5">--}}
                                    {{--<input type="text" class="form-control" id="feature-value">--}}
                                {{--</div>--}}
                                {{--<div class="col-md-2">--}}
                                    {{--<button class="btn btn-primary" id="feature-add">Add</button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group">
                        <label for="map">Posisi Pada Peta</label>
                        <div id="map" style="height: 400px">

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Foto Properti</h4>
                                </div>
                                <div class="card-body">
                                    <div class="dropzone" id="galleries">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple/>
                                            <input type="hidden" name="filename" id="filename">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="latitude" id="latitude">
                    <input type="hidden" name="longitude" id="longitude">
                    <button class="btn btn-primary" id="btn--submit">Submit</button>
                    {{--<input type="submit" value="Submit" class="btn btn-primary float-right">--}}
                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection


@section('js')
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
crossorigin=""></script>
<script src="{{ asset('dashboard-assets/modules/js/select2.full.min.js') }}"></script>
<script src="{{ asset('dashboard-assets/modules/js/dropzone.min.js') }}"></script>`

<script>
    $('#edit-button').on('click', function () {
        window.location = '/mitra/listing/edit/';
    });

    $('#province').on('change', () => {
        $('#regency')
        .find('option')
        .remove()
        .end();
        $.ajax({
            url: '{{ url('/ajax/kabupaten') }}/' + $('#province').val(),
            success: (response) => {
                $.each(response, (key, val) => {
                    $('#regency')
                    .append($("<option></option>")
                        .attr("value", val["id"])
                        .text(toTitleCase(val["name"])));
                });

                $("#regency").selectric().change();
            }
        });
    }).change();

    $('#regency').on('change', () => {
        $('#district')
        .find('option')
        .remove()
        .end();
        $.ajax({
            url: '{{ url('/ajax/kecamatan') }}/' + $('#regency').val(),
            success: (response) => {
                $.each(response, (key, val) => {
                    $('#district')
                    .append($("<option></option>")
                        .attr("value", val["id"])
                        .text(toTitleCase(val["name"])));
                });

                $("#district").selectric();
            }
        });
    }).change();

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    function getRemainFeature(id) {
        $.ajax({
            url: '/ajax/listing/remain-house-feature/' + id,
            success: (response) => {
                $.each(response, function (key, value) {
                    $('#feature-id')
                    .append($("<option></option>")
                        .attr("value", response[key]["id"])
                        .attr('data-type', response[key]["type"])
                        .text(response[key]["name"]));
                    $('#feature-id').change();
                });
            }
        });
    }

    $(document).ready(() => {
        $(function () {
            var curLocation = [0, 0];
            if (curLocation[0] == 0 && curLocation[1] == 0) {
                navigator.geolocation.getCurrentPosition(function (location) {
                    var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                    console.log(latlng);
                    curLocation = [latlng['lat'], latlng['lng']];
                    $('#latitude').val(latlng['lat']);
                    $('#longitude').val(latlng['lng']);
                    var map = L.map('map').setView(curLocation, 10);

                    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                        attribution: ''
                    }).addTo(map);

                    map.attributionControl.setPrefix(false);

                    var marker = new L.marker(curLocation, {
                        draggable: 'true'
                    });

                    marker.on('dragend', function (event) {
                        var position = marker.getLatLng();
                        marker.setLatLng(position, {
                            draggable: 'true'
                        }).bindPopup(position).update();
                        $("#latitude").val(position.lat);
                        $("#longitude").val(position.lng).keyup();
                    });

                    $("#Latitude, #Longitude").change(function () {
                        var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
                        marker.setLatLng(position, {
                            draggable: 'true'
                        }).bindPopup(position).update();
                        map.panTo(position);
                    });

                    map.addLayer(marker);
                });
            }
        });

        getRemainFeature();
    });

    $('#feature-id').on('change', () => {
        val = $('#feature-id').find(':selected').data('type');
        var placeholder = "";
        if (val == 1) {
            placeholder = "1 untuk ada, 0 untuk tidak ada";
        } else if (val == 2) {
            placeholder = "Masukan angka";
        } else {
            placeholder = "Masukan jawaban";
        }
        $('#feature-value').attr('placeholder', placeholder);
    });
    $('#feature-add').on('click', (e) => {
        e.preventDefault();
        $.ajax({
            url: '/mitra/listing/feature/add/' + '/' + $('#feature-id').val() + '/' + $('#feature-value').val(),
            success: (response) => {
                getRemainFeature('');
                getCurrentFeature('');
            }
        })
    });
</script>

<script>
        // let form = $('#form__');
        // Dropzone.options.form = {
        //     autoProcessQueue: false,
        //     uploadMultiple: true,
        //     init: function (e) {
        //
        //         var myDropzone = this;
        //
        //         $('#btn_upload').on("click", function() {
        //             myDropzone.processQueue(); // Tell Dropzone to process all queued files.
        //         });
        //
        //         // Event to send your custom data to your server
        //         myDropzone.on("sending", function(file, xhr, data) {
        //
        //             // First param is the variable name used server side
        //             // Second param is the value, you can add what you what
        //             // Here I added an input value
        //             data.append("your_variable", $('#your_input').val());
        //         });
        //
        //     }
        // };
        var dropzone = new Dropzone("#galleries", {
            autoProcessQueue: false,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            acceptedFiles: ".jpeg,.jpg,.png",
            url: "/mitra/listing/image/upload/",
            success: function (file, response) {

                // file.upload.filename = response.path;
            },
            complete: () => {
                $('#fire-modal-1').modal('hide');
                window.location.href = "/mitra/listing";
            },
            init: function () {
                // var data = "";
                // var existingFiles = JSON.parse(data.replace(/&quot;/g,'"'));
                // for (i = 0; i < existingFiles.length; i++) {
                //     var f = { name: existingFiles[i]["path"], size: 12345678 };
                //     this.addFile.call(this, f);
                //     this.emit("addedfile", f);
                //     this.createThumbnailFromUrl(f, "/storage/" + existingFiles[i]["thumbnail"]);
                //     this.emit("thumbnail", f, "/storage/" + existingFiles[i]["thumbnail"]);
                //     this.emit("success", f);
                //     this.emit("complete", f);
                //     this.files.push(f);
                // }

            }
        });
    </script>
    <script>
        $("#btn--submit").fireModal({
            title: 'Please Wait...',
            body: '<div class="text-center"><div class="lds-hourglass"></div> <br> Submitting...</div>',
            center: true,
            closeButton: false
        });

        $('#btn--submit').on('click', function () {
            $('#form__').ajaxSubmit({
                url: $(this).attr('action'),
                type: 'post',
                success: (response) => {
                    dropzone.options.url = dropzone.options.url + response;
                    dropzone.options.parallelUploads = dropzone.files.length;
                    dropzone.processQueue();
                },
                error: (message) => {
                    setTimeout(() => {
                        toastr.error(message.responseJSON.message);
                        $('#fire-modal-1').modal('hide');
                    }, 1000);
                }
            });
        });
    </script>
    <script src="{{ asset('dashboard-assets/modules/js/jquery.selectric.min.js') }}"></script>
    {{--    <script src="{{ asset('js/jquery.dm-uploader.js') }}"></script>--}}
    @endsection