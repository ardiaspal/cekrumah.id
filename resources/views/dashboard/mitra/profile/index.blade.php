@extends('dashboard.layouts.app')

@section('title', 'Home')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/summernote-bs4.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profile</h1>
            <div class="section-header-breadcrumb">
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Hi, {{ $user->name }}!</h2>
            <p class="section-lead">
                Ganti informasi tentang anda disini
            </p>

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-5">
                    <div class="card profile-widget">
                        <div class="profile-widget-header">
                            <img alt="image"
                                 src="{{ is_null($user->info->avatar) ? asset('dashboard-assets/img/avatar/avatar-1.png') : asset('storage/' . $user->info->avatar) }}"
                                 class="rounded-circle profile-widget-picture">
                            <div class="profile-widget-items">
                                <div class="profile-widget-item">
                                    <div class="profile-widget-item-label">Listing</div>
                                    <div class="profile-widget-item-value">{{ $user->listings->count() }}</div>
                                </div>
                                {{--<div class="profile-widget-item">--}}
                                {{--<div class="profile-widget-item-label">Following</div>--}}
                                {{--<div class="profile-widget-item-value">2,1K</div>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <div class="profile-widget-description">
                            <div class="profile-widget-name"><strong>{{ $user->name }}</strong></div>
                            {!! $user->description !!}
                        </div>
                        <div class="card-footer text-center">
                            {{--<div class="font-weight-bold mb-2">Follow Ujang On</div>--}}
                            {{--<a href="#" class="btn btn-social-icon btn-facebook mr-1">--}}
                            {{--<i class="fab fa-facebook-f"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#" class="btn btn-social-icon btn-twitter mr-1">--}}
                            {{--<i class="fab fa-twitter"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#" class="btn btn-social-icon btn-github mr-1">--}}
                            {{--<i class="fab fa-github"></i>--}}
                            {{--</a>--}}
                            {{--<a href="#" class="btn btn-social-icon btn-instagram">--}}
                            {{--<i class="fab fa-instagram"></i>--}}
                            {{--</a>--}}
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-7">
                    <div class="card">
                        <form method="post" class="needs-validation" action="{{ route('mitra.profile.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4>Edit Profile</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-12 col-12">
                                        <label>Name</label>
                                        <input type="text" class="form-control" value="{{ $user->name }}" required=""
                                               name="name">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-7 col-12">
                                        <label>Email</label>
                                        <input type="email" class="form-control" value="{{ $user->email }}" disabled>
                                    </div>
                                    <div class="form-group col-md-5 col-12">
                                        <label>Phone</label>
                                        <input type="tel" class="form-control" value="{{ $user->info->no_hp }}" name="no_hp">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12 col-12">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control" value="{{ $user->info->alamat }}"
                                               name="alamat">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12 col-md-12">
                                        <label>Avatar</label>
                                        <div class="custom-file">
                                            <input type="file" name="avatar" class="custom-file-input" id="site-logo">
                                            <label class="custom-file-label">Choose File</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Deskripsi</label>
                                        <textarea class="form-control summernote-simple"
                                                  name="description">{!! $user->info->description !!}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary">Save Changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js_')
    <script src="{{ asset('dashboard-assets/modules/js/summernote-bs4.js') }}"></script>
@endsection