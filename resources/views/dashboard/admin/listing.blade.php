@extends('dashboard.layouts.app')

@section('title', 'Listing')

@section('css')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>DataTables</h1>
            <div class="section-header-button">
                <a href="{{ route('admin.listing.create') }}" class="btn btn-primary">Add New</a>
            </div>
            <div class="section-header-breadcrumb">
            </div>
        </div>
        <form action="{{ route('admin.listing.update.status') }}" method="post" id="action-form">
            @csrf
            <input type="hidden" name="listing" value="-1" id="listing-update-id">
            <input type="hidden" id="status" name="status">
        </form>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Listing</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="listing-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Judul</th>
                                        <th>Luas Tanah</th>
                                        <th>Luas Bangunan</th>
                                        <th>Provinsi</th>
                                        <th>Harga</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- JS Libraies -->
    <script src="{{ asset('dashboard-assets/modules/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/select.bootstrap4.min.js') }}"></script>

    <script>
        $('#listing-table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            ajax: "{{ route('ajax.admin.listing') }}",
            createdRow: function(row, data, index){
                $(row).addClass('clickable-row');
                $(row).attr('data-href', '/admin/listing/detail/' + data['id']);
            },
            columns: [
                {data: 'no', name: 'no'},
                {data: 'judul', name: 'judul'},
                {data: 'l_tanah', name: 'l_tanah'},
                {data: 'l_bangunan', name: 'l_bangunan'},
                {data: 'provinsi', name: 'provinsi'},
                {data: 'harga_format', name: 'harga_format'},
                {data: 'action', name: 'action'},
            ],
            drawCallback: function(){
                $('.clickable-row td').on('click', function(){
                    if($(this).children('button').length){
                        return;
                    }
                    window.location = $(this).parent().data('href');
                });

                $('#action-button').on('click', function(){
                    $('#status').val($(this).data('id'));
                    $('#listing-update-id').val($(this).parent().data('listing-id'));
                    $('#action-form').submit();
                });

                $('#block-button').on('click', function(){
                    $('#status').val($(this).data('id'));
                    $('#listing-update-id').val($(this).parent().data('listing-id'));
                    $('#action-form').submit();
                });
            }
        });
    </script>
@endsection