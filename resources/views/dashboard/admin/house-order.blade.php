@extends('dashboard.layouts.app')

@section('title', 'Listing')

@section('css')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Pesanan</h1>
            <div class="section-header-breadcrumb">
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Pesanan</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="order-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Pembeli</th>
                                        <th>Rumah</th>
                                        <th>Harga</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- JS Libraies -->
    <script src="{{ asset('dashboard-assets/modules/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/select.bootstrap4.min.js') }}"></script>

    <script>
        $('#order-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('ajax.admin.order') }}",
            createdRow: function(row, data, index){
                $(row).addClass('clickable-row');
                $(row).attr('data-href', '/admin/house-order/detail/' + data['id']);
            },
            columns: [
                {data: 'no', name: 'no'},
                {data: 'buyer', name: 'buyer'},
                {data: 'rumah', name: 'rumah'},
                {data: 'harga', name: 'harga'},
                {data: 'status_', name: 'status_'},
            ],
            drawCallback: function(){
                $('.clickable-row').on('click', function(){
                    window.location = $(this).data('href');
                });
            }
        });
    </script>
@endsection