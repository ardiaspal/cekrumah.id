@extends('dashboard.layouts.app')

@section('title', 'Pesanan Detail ' . $order->id)

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
    <link rel="stylesheet" href="{{ asset('css/chat-style.css') }}">
@endsection

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{ $order->listing->title }}</h4>
                        <small>{{ date('F, d Y | H:i:s', strtotime($order->listing->created_at)) }}</small>
                        <br>
                        <br>
                        <!--Carousel Wrapper-->
                        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                            <!--Indicators-->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                            </ol>
                            <!--/.Indicators-->
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                <!--First slide-->
                                <div class="carousel-item active">
                                    <img class="d-block w-100"
                                         src="https://mdbootstrap.com/img/Photos/Slides/img%20(130).jpg"
                                         alt="First slide">
                                </div>
                                <!--/First slide-->
                                <!--Second slide-->
                                <div class="carousel-item">
                                    <img class="d-block w-100"
                                         src="https://mdbootstrap.com/img/Photos/Slides/img%20(129).jpg"
                                         alt="Second slide">
                                </div>
                                <!--/Second slide-->
                                <!--Third slide-->
                                <div class="carousel-item">
                                    <img class="d-block w-100"
                                         src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg"
                                         alt="Third slide">
                                </div>
                                <!--/Third slide-->
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-example-1z" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-1z" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                        </div>
                        <br>
                        <hr>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <h2>{{ price_format($order->listing->harga) }}</h2>
                                </div>
                                <div class="col-md-4" style="vertical-align: center">
                                    Luas Bangunan : {{ $order->listing->luas_bangunan }} | Luas Tanah
                                    : {{ $order->listing->luas_tanah }}
                                </div>
                                <div class="col-md-4">
                                    <button class="btn btn-primary">
                                        {{ ucfirst(strtolower($order->listing->district->name)) }}
                                        , {{ ucfirst(strtolower($order->listing->district->regency->name)) }}
                                    </button>
                                </div>
                            </div>
                        </div>

                        <br><hr>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="col-md-12">

                                        <h4>Detail Properti</h4>
                                        <br>
                                        {{ $order->listing->deskripsi }}
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h4>Informasi Properti</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table class="table">
                                                <tbody>
                                                <tr>
                                                    <td>Daya Listrik</td>
                                                    <td>:</td>
                                                    <td>{{ $order->listing->listrik }} W</td>
                                                </tr>
                                                @foreach($order->listing->houseFeatureDetails as $feature)
                                                    <tr>
                                                        <td>{{ $feature->houseFeature->name }}</td>
                                                        <td>:</td>
                                                        <td>{{ $feature->value }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/.Carousel Wrapper-->

                        <div class="col-md-12">
                            <div class="container">
                                <h3>Posisi Peta</h3>
                                <div id="map" style="height: 400px">

                                </div>
                            </div>
                        </div>

                        <br><br>

                        <div class="col-md-12">
                            <div class="container">
                                <h3>Chat Dengan Admin</h3>
                                <div class="chat-body">
                                    @foreach($messages as $message)
                                        <div class="chat-container {{ $message->user_id == Auth::id() ? 'darker' : '' }}">
                                            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8E3AljbKpwWcD7doIx2ZdZEPfDw8bEpdhipIRXx2gCeip8EKrFw" alt="Avatar" style="width:100%;" class="{{ $message->user_id == Auth::id() ? 'right' : 'left' }}"> <strong>{{ $message->user->name }}</strong>
                                            <p>{{ $message->message }}</p>
                                            <span class="time-{{ $message->user != Auth::id() ? 'right' : 'left' }}">{{ $message->created_at->format('H:i:s d/m/Y') }}</span>
                                        </div>
                                    @endforeach

                                    {{--<div class="chat-container darker">--}}
                                    {{--<img src="https://yt3.ggpht.com/a/AGF-l79Cs_gmVE6JjJ7goHw-7F1HGz4k9onfgQujlg=s900-c-k-c0xffffffff-no-rj-mo" alt="Avatar" class="right" style="width:100%;">--}}
                                    {{--<p>Hey! I'm fine. Thanks for asking!</p>--}}
                                    {{--<span class="time-left">11:01</span>--}}
                                    {{--</div>--}}

                                    {{--<div class="chat-container">--}}
                                    {{--<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8E3AljbKpwWcD7doIx2ZdZEPfDw8bEpdhipIRXx2gCeip8EKrFw" alt="Avatar" style="width:100%;">--}}
                                    {{--<p>Sweet! So, what do you wanna do today?</p>--}}
                                    {{--<span class="time-right">11:02</span>--}}
                                    {{--</div>--}}
                                </div>
                                <br><br>
                                <form action="{{ route('member.house-order.send-messages', $order) }}" method="post">
                                    @csrf
                                    <div class="form-group" >
                                        <textarea name="message" id="" rows="5" class="form-control"></textarea>
                                    </div>
                                    <button class="btn btn-primary float-right" type="submit">Kirim</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin=""></script>
    <script>
        $(document).ready(() => {
            $(function () {
                var curLocation = [0, 0];
                if (curLocation[0] == 0 && curLocation[1] == 0) {
                    navigator.geolocation.getCurrentPosition(function (location) {
                        // var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                        var latlng = JSON.parse('{!! $order->listing->lokasi !!}');
                        curLocation = [latlng['latitude'], latlng['longitude']];
                        var map = L.map('map').setView(curLocation, 10);

                        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                            attribution: ''
                        }).addTo(map);

                        map.attributionControl.setPrefix(false);

                        var marker = new L.marker(curLocation, {

                        });

                        marker.on('dragend', function (event) {
                            var position = marker.getLatLng();
                            marker.setLatLng(position, {
                                draggable: 'false'
                            }).bindPopup(position).update();
                            $("#latitude").val(position.lat);
                            $("#longitude").val(position.lng).keyup();
                        });

                        map.addLayer(marker);
                    });
                }
            });
        });
    </script>
@endsection