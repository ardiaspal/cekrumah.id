@extends('dashboard.layouts.app')

@section('title', 'Listing')

@section('css')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select.bootstrap4.min.css') }}">
@endsection

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Sarana</h1>
            <div class="section-header-button">
                <a href="{{ route('admin.house-feature.create') }}" class="btn btn-primary">Add New</a>
            </div>
            <div class="section-header-breadcrumb">
            </div>
        </div>

        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Data Sarana</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="feature-table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">
                                            #
                                        </th>
                                        <th>Sarana</th>
                                        <th>Jenis Data</th>
                                        <th>Jumlah Listing</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <!-- JS Libraies -->
    <script src="{{ asset('dashboard-assets/modules/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/select.bootstrap4.min.js') }}"></script>

    <script>
        $('#add-button').on('click', function () {
            window.location = '{{ route('admin.house-feature.create')  }}'
        });

        $('#feature-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('ajax.admin.house-feature') }}",
            columns: [
                {data: 'no', name: 'no'},
                {data: 'name', name: 'name'},
                {data: 'type', name: 'type'},
                {data: 'jumlah', name: 'jumlah'},
                {data: 'delete', name: 'delete'},
            ]
        });

        function deleteData(id) {
            $('#delete-id').val(id);
            $('#delete-form').submit();
        }
    </script>
@endsection

{{--@extends('dashboard.layouts.app')--}}

{{--@section('title', 'Sarana')--}}

{{--@section('css')--}}

{{--@endsection--}}

{{--@section('content')--}}
    {{--<div class="main-content-inner">--}}
        {{--<div class="row">--}}
            {{--<div class="col-12 mt-5">--}}
                {{--<div class="card">--}}
                    {{--<div class="card-body">--}}
                        {{--<button class="btn btn-primary float-right" id="add-button">--}}
                            {{--Tambah--}}
                        {{--</button>--}}
                        {{--<br>--}}
                        {{--<h4 class="header-title">Sarana</h4>--}}
                        {{--<div class="data-tables">--}}
                            {{--<table id="sarana-table" class="text-center">--}}
                                {{--<thead class="bg-light text-capitalize">--}}
                                {{--<tr>--}}
                                    {{--<th>No</th>--}}
                                    {{--<th>Sarana</th>--}}
                                    {{--<th>Jenis Data</th>--}}
                                    {{--<th>Jumlah Listing</th>--}}
                                    {{--<th>Action</th>--}}
                                {{--</tr>--}}
                                {{--</thead>--}}
                                {{--<tbody>--}}

                                {{--</tbody>--}}
                            {{--</table>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<form action="{{ route('admin.house-feature.destroy') }}" id="delete-form" method="post">--}}
        {{--@csrf--}}
        {{--@method('DELETE')--}}
        {{--<input type="hidden" id="delete-id" name="id">--}}
    {{--</form>--}}
{{--@endsection--}}


{{--@section('js')--}}
    {{--<script>--}}
        {{--$('#add-button').on('click', function(){--}}
            {{--window.location = '{{ route('admin.house-feature.create')  }}'--}}
        {{--});--}}

        {{--$('#sarana-table').DataTable({--}}
            {{--processing: true,--}}
            {{--serverSide: true,--}}
            {{--ajax: "{{ route('ajax.admin.house-feature') }}",--}}
            {{--columns: [--}}
                {{--{data: 'no', name: 'no'},--}}
                {{--{data: 'name', name: 'name'},--}}
                {{--{data: 'type', name: 'type'},--}}
                {{--{data: 'jumlah', name: 'jumlah'},--}}
                {{--{data: 'delete', name: 'delete'},--}}
            {{--]--}}
        {{--});--}}
        {{----}}
        {{--function deleteData(id) {--}}
            {{--$('#delete-id').val(id);--}}
            {{--$('#delete-form').submit();--}}
        {{--}--}}
    {{--</script>--}}
{{--@endsection--}}