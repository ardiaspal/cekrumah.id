@extends('dashboard.layouts.app')

@section('title', 'Home')

@section('css-library')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>

    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/dropzone.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard-assets/modules/css/select2.min.css') }}">
@endsection

@section('css')

@endsection

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4>Edit Listing</h4>
                        <br>
                        <form action="{{ route('admin.listing.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Judul</label>
                                <input type="text" class="form-control" id="title" name="title"
                                       value="">
                            </div>
                            <div class="form-group">
                                <label for="">Dijual atau Disewakan</label>
                                <br>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="jenis" class="custom-control-input" id="dijual-radio"
                                            value="1">
                                    <label class="custom-control-label" for="dijual-radio">Dijual</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" name="jenis" class="custom-control-input" id="disewakan"
                                            value="0">
                                    <label class="custom-control-label" for="disewakan">Disewakan</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="luas-tanah">Luas Tanah</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="luas_tanah" id="luas-tanah"
                                           value="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="luas-tanah">m²</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="luas-bangunan">Luas Bangunan</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="luas_bangunan" id="luas-bangunan"
                                           value="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="luas-bangunan">m²</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga">Harga</label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">Rp</span>
                                    </div>
                                    <input type="number" id="harga" name="harga" class="form-control"
                                           aria-describedby="basic-addon1" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="listrik">Daya Listrik</label>
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="listrik" id="listrik"
                                           value="">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="listrik">Watt</span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Provinsi</label>
                                <select name="provinsi" id="provinsi-select" class="form-control">
                                    @foreach($provinces as $province)
                                        <option value="{{ $province->id }}">{{ $province->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Kabupaten</label>
                                <select name="kabupaten" id="kabupaten-select" class="form-control">

                                </select>
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Kecamatan</label>
                                <select name="district_id" id="kecamatan-select" class="form-control">

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alamat">Alamat</label>
                                <input type="text" class="form-control" id="alamat" name="alamat"
                                       value="">
                            </div>

                            <div class="form-group">
                                <label for="deskripsi">Deskripsi</label>
                                <textarea class="form-control" aria-label="With textarea" name="deskripsi"
                                          id="deskripsi" rows="10"></textarea>
                            </div>

                            <div class="form-group">
                                <label for="kamar-mandi">Kamar Mandi</label>
                                <input type="number" class="form-control" id="kamar-mandi" name="kamar_mandi"
                                       value="">
                            </div>

                            <div class="form-group">
                                <label for="kamar-tidur">Kamar Tidur</label>
                                <input type="number" class="form-control" id="kamar-tidur" name="kamar_tidur"
                                       value="">
                            </div>

                            <div class="form-group">
                                <label for="map">Sarana</label>
                                <div class="col-md-12">
                                    <table class="table-bordered table" id="feature-table">
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <select id="feature-id" class="form-control">

                                            </select>
                                        </div>
                                        <div class="col-md-5">
                                            <input type="text" class="form-control" id="feature-value">
                                        </div>
                                        <div class="col-md-2">
                                            <button class="btn btn-primary" id="feature-add">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="map">Posisi Pada Peta</label>
                                <div id="map" style="height: 400px">

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h4>Multiple Upload</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="dropzone" id="galleries">
                                                <div class="fallback">
                                                    @csrf
                                                    <input name="file" type="file" multiple/>
                                                    <input type="hidden" name="filename" id="filename">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="latitude" id="latitude">
                            <input type="hidden" name="longitude" id="longitude">
                            <input type="submit" value="Submit" class="btn btn-primary float-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin=""></script>
    <script src="{{ asset('dashboard-assets/modules/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('dashboard-assets/modules/js/dropzone.min.js') }}"></script>`
    <script>
        $('#edit-button').on('click', function () {
            window.location = '/admin/listing/edit/';
        });
        $(document).ready(() => {
            $('#province-select').select2();
        });
        $('#provinsi-select').on('change', function () {
            $('#kabupaten-select')
                .find('option')
                .remove()
                .end();

            $.ajax({
                url: '/ajax/kabupaten/' + $(this).val(),
                success: (response) => {
                    $.each(response, function (key, value) {
                        $('#kabupaten-select')
                            .append($("<option></option>")
                                .attr("value", response[key]["id"])
                                .text(response[key]["name"]));
                    });
                }
            })
        });
        $('#provinsi-select').change();
        $('#kabupaten-select').on('change', function () {
            $('#kecamatan-select')
                .find('option')
                .remove()
                .end();

            $.ajax({
                url: '/ajax/kecamatan/' + $(this).val(),
                success: (response) => {
                    $.each(response, function (key, value) {
                        $('#kecamatan-select')
                            .append($("<option></option>")
                                .attr("value", response[key]["id"])
                                .text(response[key]["name"]));
                    });
                }
            })
        });
        $(document).ready(() => {
            $(function () {
                var curLocation = [0, 0];
                if (curLocation[0] == 0 && curLocation[1] == 0) {
                    navigator.geolocation.getCurrentPosition(function (location) {
                        var latlng = new L.LatLng(location.coords.latitude, location.coords.longitude);
                        console.log(latlng);
                        curLocation = [latlng['lat'], latlng['lng']];
                        $('#latitude').val(latlng['lat']);
                        $('#longitude').val(latlng['lng']);
                        var map = L.map('map').setView(curLocation, 10);

                        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                            attribution: ''
                        }).addTo(map);

                        map.attributionControl.setPrefix(false);

                        var marker = new L.marker(curLocation, {
                            draggable: 'true'
                        });

                        marker.on('dragend', function (event) {
                            var position = marker.getLatLng();
                            marker.setLatLng(position, {
                                draggable: 'true'
                            }).bindPopup(position).update();
                            $("#latitude").val(position.lat);
                            $("#longitude").val(position.lng).keyup();
                        });

                        $("#Latitude, #Longitude").change(function () {
                            var position = [parseInt($("#Latitude").val()), parseInt($("#Longitude").val())];
                            marker.setLatLng(position, {
                                draggable: 'true'
                            }).bindPopup(position).update();
                            map.panTo(position);
                        });

                        map.addLayer(marker);
                    });
                }
            });

            getRemainFeature();
        });

        function getRemainFeature(id) {
            $.ajax({
                url: '/ajax/admin/listing/remain-house-feature/null',
                success: (response) => {
                    $.each(response, function (key, value) {
                        $('#feature-id')
                            .append($("<option></option>")
                                .attr("value", response[key]["id"])
                                .attr('data-type', response[key]["type"])
                                .text(response[key]["name"]));
                        $('#feature-id').change();
                    });
                }
            });
        }

        function getCurrentFeature(id) {
            $.ajax({
                url: '/ajax/admin/listing/house-feature/' + id,
                success: (response) => {
                    $('#feature-table').find('tbody').detach();
                    $('#feature-table').append($('<tbody><tr></tr></tbody>'));
                    $.each(response, (key, val) => {
                        var feature = response[key]["house_feature"]["name"];
                        var value = response[key]["value"];
                        var feature_id = response[key]["house_feature"]["id"];
                        var button = '<button class="btn btn-danger" onclick"deleteFeature(' + feature_id + ')">Delete</button>'
                        $('#feature-table tr:last').after('<tr>' +
                            '<td>' + feature + '</td>' +
                            '<td>' + value + '</td>' +
                            '<td>' + button + '</td>' +
                            '</tr>');
                    });
                }
            });
        }

        $('#feature-id').on('change', () => {
            val = $('#feature-id').find(':selected').data('type');
            var placeholder = "";
            if (val == 1) {
                placeholder = "1 untuk ada, 0 untuk tidak ada";
            } else if (val == 2) {
                placeholder = "Masukan angka";
            } else {
                placeholder = "Masukan jawaban";
            }
            $('#feature-value').attr('placeholder', placeholder);
        });
        $('#feature-add').on('click', (e) => {
            e.preventDefault();
            $.ajax({
                url: '/admin/listing/feature/add/' + '/' + $('#feature-id').val() + '/' + $('#feature-value').val(),
                success: (response) => {
                    getRemainFeature('');
                    getCurrentFeature('');
                }
            })
        });
    </script>

    <script>
        var dropzone = new Dropzone("#galleries", {
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            acceptedFiles: ".jpeg,.jpg,.png",
            url: "/admin/listing/image/upload/",
            removedfile: function (file) {
                var name = file.upload.filename;
                console.log(file.upload);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: '/admin/listing/image/remove/',
                    data: {filename: name},
                    success: function (data) {
                        console.log("File has been successfully removed!!");
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ? fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function (file, response) {
                file.upload.filename = response.path;
            },
            init: function () {
                // var data = "";
                // var existingFiles = JSON.parse(data.replace(/&quot;/g,'"'));
                // for (i = 0; i < existingFiles.length; i++) {
                //     var f = { name: existingFiles[i]["path"], size: 12345678 };
                //     this.addFile.call(this, f);
                //     this.emit("addedfile", f);
                //     this.createThumbnailFromUrl(f, "/storage/" + existingFiles[i]["thumbnail"]);
                //     this.emit("thumbnail", f, "/storage/" + existingFiles[i]["thumbnail"]);
                //     this.emit("success", f);
                //     this.emit("complete", f);
                //     this.files.push(f);
                // }
                {{--                    @foreach(as $index => $upload)--}}
                {{--                        var mockFile = { name: "{{ $upload->path }}", size: 12345, thumbnail: "{{ url('/storage/' . $upload->thumbnail) }}"};--}}
                {{--                        this.addFile.call(this, mockFile);--}}
                {{--                        this.options.thumbnail.call(this, mockFile, "{{ url('/storage/' . $upload->thumbnail) }}");--}}
                {{--                    @endforeach--}}

            }
        });
    </script>

    {{--    <script src="{{ asset('js/jquery.dm-uploader.js') }}"></script>--}}
@endsection