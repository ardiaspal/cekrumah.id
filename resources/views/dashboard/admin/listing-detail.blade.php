@extends('dashboard.layouts.app')

@section('title', 'Home')

@section('css')

@endsection

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <button class="btn btn-warning float-right" id="edit-button">
                            Edit
                        </button>
                        <form action="{{ route('admin.listing.update.status') }}" method="post" id="action-form">
                            @csrf
                            <input type="hidden" name="listing" value="{{ $listing->id }}">
                            <input type="hidden" id="status" name="status">
                        </form>
                        @if($listing->status == 1)
                            <button class="btn btn-primary float-right" id="action-button" data-id="2">Publish</button>
                            <button class="btn btn-danger float-right" id="block-button" data-id="4">Block</button>
                        @elseif($listing->status == 2)
                            <button class="btn btn-danger float-right" id="action-button" data-id="3">Nonaktifkan</button>
                            <button class="btn btn-danger float-right" id="block-button" data-id="4">Block</button>
                        @elseif($listing->status == 3)
                            <button class="btn btn-success float-right" id="action-button" data-id="2">Aktifkan</button>
                            <button class="btn btn-danger float-right" id="block-button" data-id="4">Block</button>
                        @elseif($listing->status == 4)
                            <button class="btn btn-success float-right" id="action-button" data-id="2">Unblock</button>
                        @endif
                        <h4 class="header-title">{{ $listing->title }}</h4>
                        <small>{{ date('F, d Y | H:i:s', strtotime($listing->created_at)) }}</small>
                        <br>
                        <br>
                        <!--Carousel Wrapper-->
                        <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                            <!--Indicators-->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                                <li data-target="#carousel-example-1z" data-slide-to="2"></li>
                            </ol>
                            <!--/.Indicators-->
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                @foreach($listing->uploads as $index => $image)
                                    <div class="carousel-item {{ $index == 0 ? 'active' : '' }}">
                                        <img class="d-block w-100"
                                             src="{{ asset('storage/'. $image->path) }}" height="500px"
                                             alt="">
                                    </div>
                                @endforeach
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-example-1z" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-1z" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                        </div>
                        <br>
                        <hr>
                        <!--/.Carousel Wrapper-->
                        <h4>Detail</h4>
                        <br>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>Jenis Listing</td>
                                <td>{{ $listing->jenis == 1 ? 'Dijual' : 'Disewakan' }}</td>
                            </tr>
                            <tr>
                                <td>Luas Tanah</td>
                                <td>{{ $listing->luas_tanah }} m</td>
                            </tr>
                            <tr>
                                <td>Luas Bangunan</td>
                                <td>{{ $listing->luas_bangunan }} m</td>
                            </tr>
                            <tr>
                                <td>Harga</td>
                                <td>{{ price_format($listing->harga) }}</td>
                            </tr>
                            <tr>
                                <td>Daya Listrik</td>
                                <td>{{ $listing->listrik }} W</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>{{ $listing->alamat }} </td>
                            </tr>
                            <tr>
                                <td>Lokasi</td>
                                <td>
                                    Longtitude : {{ json_decode($listing->lokasi, true)["longitude"] }} <br>
                                    Langtitude : {{ json_decode($listing->lokasi, true)["latitude"] }}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <h4>Deskripsi</h4>
                        <p>{{ $listing->deskripsi }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>
        $('#edit-button').on('click', function () {
            window.location = '/admin/listing/edit/{{ $listing->id }}';
        });

        $('#action-button').on('click', function(){
            $('#status').val($(this).data('id'));
            $('#action-form').submit();
        });

        $('#block-button').on('click', function(){
            $('#status').val($(this).data('id'));
            $('#action-form').submit();
        });
    </script>
@endsection