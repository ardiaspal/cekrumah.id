@extends('dashboard.layouts.app')

@section('title', 'Tambah Sarana')

@section('css')

@endsection

@section('content')
    <div class="main-content-inner">
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <h4>Tambah Sarana</h4>
                        <br>
                        <form action="{{ route('admin.house-feature.store') }}" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="title">Nama Sarana</label>
                                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Tipe Sarana</label>
                                <select name="type" id="type" class="form-control">
                                    @foreach($types as $index => $type)
                                        <option value="{{ $index }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" value="Submit" class="btn btn-primary float-right">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('js')
    <script>

    </script>
@endsection