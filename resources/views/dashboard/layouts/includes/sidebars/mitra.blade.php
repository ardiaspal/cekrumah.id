<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Cekrumah</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">CRM</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Menu</li>
            <li class="nav-item dropdown">
                <a href="{{ route('mitra.dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ route('mitra.listing.index')  }}" class="nav-link"><i class="fas fa-home"></i><span>Daftar Rumah</span></a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ route('mitra.profile.index')  }}" class="nav-link"><i class="fas fa-user"></i><span>Profile</span></a>
            </li>
        </ul>
    </aside>
</div>