<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ url('/') }}">Cekrumah</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/') }}">CRM</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Menu</li>
            <li class="nav-item dropdown">
                <a href="{{ route('admin.dashboard') }}" class="nav-link"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ route('admin.listing')  }}" class="nav-link"><i class="fas fa-home"></i><span>Daftar Rumah</span></a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ route('admin.house-order') }}" class="nav-link"><i class="fas fa-receipt"></i><span>Pesanan</span></a>
            </li>
            <li class="nav-item dropdown">
                <a href="{{ route('admin.house-feature') }}" class="nav-link"><i class="fas fa-fire"></i><span>Sarana</span></a>
            </li>
        </ul>
    </aside>
</div>