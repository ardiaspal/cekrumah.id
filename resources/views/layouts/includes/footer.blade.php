<footer class="dark-footer skin-dark-footer">
    <div>
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Tentang Cekrumah</h4>
                        <p>Cekrumah merupakan situs untuk akses jual beli rumah dan properti pertama yang menggunakan fitur cek properti, jadi anda bisa cek properti yang akan dibeli!
                            <br>
                            Let's check your paradise!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Useful links</h4>
                        <ul class="footer-menu">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('sell') }}">Daftar Rumah</a></li>
                            <li><a href="{{ route('cekrumah') }}">Cekrumah</a></li>
                            <li><a href="{{ route('register') }}">Daftar</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Get in Touch</h4>
                        <div class="fw-address-wrap">
                            <div class="fw fw-location">
                                Jl. Sriwijaya, Sumbersari, Jember, Jawa Timur, Indonesia
                            </div>
                            <div class="fw fw-mail">
                                cekrumah.id@gmail.com
                            </div>
                            {{--<div class="fw fw-call">--}}
                                {{--+91 254 584 7584--}}
                            {{--</div>--}}
                            {{--<div class="fw fw-skype">--}}
                                {{--drizvato77--}}
                            {{--</div>--}}
                            <div class="fw fw-web">
                                cekrumah.id
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="footer-widget">
                        <h4 class="widget-title">Follow Us</h4>
                        <p>Follow us on : </p>
                        <ul class="footer-bottom-social">
                            {{--<li><a href="index.html#"><i class="ti-facebook"></i></a></li>--}}
                            {{--<li><a href="index.html#"><i class="ti-twitter"></i></a></li>--}}
                            <li><a href="https://www.instagram.com/cekrumah.id_official"><i class="ti-instagram"></i></a></li>
                        </ul>

                        {{--<form class="f-newsletter mt-4">--}}
                            {{--<input type="email" class="form-control sigmup-me" placeholder="Your Email Address" required="required">--}}
                            {{--<button type="submit" class="btn"><i class="ti-arrow-right"></i></button>--}}
                        {{--</form>--}}
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-12 col-md-12 text-center">
                    <p class="mb-0">© 2019 Cekrumah.id</p>
                </div>

            </div>
        </div>
    </div>
</footer>