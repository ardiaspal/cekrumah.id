<div class="header header-light">
    <nav class="headnavbar">
        <div class="nav-header">
            <a href="{{ route('home') }}" class="brand"><img src="{{ asset('img/logo-light.png') }}" alt=""/></a>
            <button class="toggle-bar"><span class="ti-align-justify"></span></button>
        </div>
        <ul class="menu">

            <li>
                <a class="{{ Request::is('/') ? 'active' : '' }}" href="{{ route('home') }}">Home</a>
            </li>

            <li>
                <a class="{{ Request::is('sell') ? 'active' : '' }}" href="{{ route('sell') }}">Daftar Rumah</a>
            </li>

            <li>
                <a class="{{ Request::is('cekrumah') ? 'active' : '' }}" href="{{ route('cekrumah') }}">Cek Rumah</a>
            </li>

            @if(!Auth::user())
                <li><a href="{{ route('register') }}">Daftar</a></li>
            @else
                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
            @endif
        </ul>

        @if(!Auth::user())
            <ul class="attributes">
                <li class="login-attri theme-log">
                    <a href="{{ route('login') }}">Masuk</a>
                </li>
            </ul>
        @endif

    </nav>
</div>