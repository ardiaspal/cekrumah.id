@extends('layouts.app')

@section('title', 'Detail Rumah')

@section('css')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css"
          integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
          crossorigin=""/>
@endsection

@section('content')
    <section>
        <div class="container">
            <div class="row">

                <div class="col-lg-4 col-md-12">
                    <div class="dashboard-navbar">

                        <div class="d-user-avater">
                            <img src="{{ is_null($user->info->avatar) ? asset('dashboard-assets/img/avatar/avatar-1.png') : asset('storage/' . $user->info->avatar) }}" class="img-fluid avater" alt="">
                            <h4>{{ $user->name }}</h4>
                            <span>{{ $user->info->alamat }}</span>
                        </div>

                        <div class="d-navigation">
                            <ul>
                            </ul>
                        </div>

                    </div>
                </div>

                <div class="col-lg-8 col-md-12">
                    <div class="dashboard-wraper">
                        <div class="form-submit">
                            <h4>Properti Saya</h4>
                        </div>

                        <table class="property-table-wrap responsive-table">

                            <tbody>
                            <tr>
                                <th><i class="fa fa-file-text"></i> Property</th>
                            </tr>
                            @foreach($listings as $listing)
                                <tr>
                                    <td class="property-container">
                                        <img src="{{ asset('storage/' . $listing->uploads()->first()->path ) }}" alt="">
                                        <div class="title">
                                            <h4><a href="{{ route('detail-listing', $listing) }}">{{ $listing->title }}</a></h4>
                                            <span>{{ $listing->alamat }}</span>
                                            <span class="table-property-price">Rp {{ number_format($listing->harga, 0) }}</span>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {{ $listings->links() }}

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"
            integrity="sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og=="
            crossorigin=""></script>
    <script>
    </script>
@endsection