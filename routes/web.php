<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//{
//    Route::post('/bulk/feature', function(\Illuminate\Http\Request $request){
//        foreach ($request->all() as $req){
//            $req = (object) $req;
//            $listing = \App\Listing::where('title', $req->title)->first();
//            if(!is_null($listing)) {
//                $feature = \App\HouseFeature::where('name', $req->detail)->first();
//                if (is_null($feature)) {
//                    $feature = \App\HouseFeature::create([
//                        'id' => uid('SRN'),
//                        'name' => $req->detail,
//                        'type' => 1,
//                    ]);
//                }
//
//                \App\HouseFeatureDetail::create([
//                    'id' => uid('FD'),
//                    'house_feature_id' => $feature->id,
//                    'listing_id' => $listing->id,
//                    'value' => $req->value
//                ]);
//            }
//        }
//    });
//
//    Route::post('/bulk/details', function(\Illuminate\Http\Request $request){
////    return $request;
//        foreach ($request->all() as $req){
//            try{
////            \Illuminate\Support\Facades\DB::beginTransaction();
//                $req = (object) $req;
//                $listing = \App\Listing::where('title', $req->title)->first();
//                if(!is_null($listing)){
//                    if($req->detail == "Kamar tidur"){
//                        $listing->update([
//                            'kamar_tidur' => $req->value
//                        ]);
//                    } else if($req->detail == "Kamar mandi"){
//                        $listing->update([
//                            'kamar_mandi' => $req->value
//                        ]);
//                    } else if($req->detail == "Bangunan (m²)"){
//                        $listing->update([
//                            'luas_bangunan' => $req->value
//                        ]);
//                    } else if($req->detail == "Lahan (m²)"){
//                        $listing->update([
//                            'luas_tanah' => $req->value
//                        ]);
//                    } else {
//                        // TODO : Create or Fail House Feature
//                        $feature = \App\HouseFeature::where('name', $req->detail)->first();
//                        if(is_null($feature)){
//                            $feature = \App\HouseFeature::create([
//                                'id' => uid('SRN'),
//                                'name' => $req->detail,
//                                'type' => 3,
//                            ]);
//                        }
//
//                        \App\HouseFeatureDetail::create([
//                            'id' => uid('FD'),
//                            'house_feature_id' => $feature->id,
//                            'listing_id' => $listing->id,
//                            'value' => $req->value
//                        ]);
//                    }
////                \Illuminate\Support\Facades\DB::commit();
//                }
//            } catch (Exception $e){
//                echo $e;
////            \Illuminate\Support\Facades\DB::rollBack();
//            }
//        }
//    });
//
//}

Auth::routes();
Route::redirect('/home', '/');
Route::get('/verification/{token}', 'HomeController@verification')->name('verificaiton');
Route::get('/', 'HomeController@index')->name('home');
Route::get('/sell', 'HomeController@sell')->name('sell');
Route::get('/listing/{listing}', 'HomeController@detailListing')->name('detail-listing');
Route::get('/profile/{id}', 'HomeController@getProfile')->name('detail-profile');
Route::post('/listing/order', 'HomeController@orderListingTemp')->name('order-listing');
Route::view('/test', 'emails.test');

Route::get('cekrumah', 'HomeController@underConstruction')->name('cekrumah');

Route::get('/dashboard', function () {
    $role = \Illuminate\Support\Facades\Auth::user()->role->role;
    if ($role == "admin") {
        return redirect()->route('admin.dashboard');
    } else if ($role == "surveyor") {
        return redirect()->route('surveyor.dashboard');
    } else if ($role == "member") {
        return redirect()->route('member.dashboard');
    } else if ($role == "mitra") {
        return redirect()->route('mitra.dashboard');
    }
})->middleware('auth')->name('dashboard');

Route::group(['prefix' => 'ajax', 'as' => 'ajax.', 'namespace' => 'Ajax'], function () {
    Route::get('/kabupaten/{id}', 'HomeController@getKabupaten')->name('kabupaten');
    Route::get('/kecamatan/{id}', 'HomeController@getKota')->name('kecamatan');

    Route::get('/admin/listing', 'AdminController@getListing')->name('admin.listing');
    Route::get('/admin/order', 'AdminController@getOrder')->name('admin.order');
    Route::get('/admin/house-feature', 'AdminController@getSarana')->name('admin.house-feature');
    Route::get('/listing/remain-house-feature/{id}', 'AdminController@getRemainHouseFeatureListing')->name('admin.listing-remain-house-feature');
    Route::get('/listing/house-feature/{id}', 'AdminController@getHouseFeatureListing')->name('admin.listing-house-feature');

    Route::group(['prefix' => 'member', 'as' => 'member.'], function(){
        Route::get('/wishlist', 'MemberController@getWishlist')->name('get-wishlist');
    });

    Route::group(['prefix' => 'mitra', 'as' => 'mitra.'], function(){
        Route::get('/listing', 'MitraController@getListing')->name('listing');
    });
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['role:admin']], function () {

    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/listing', 'ListingController@index')->name('listing');
    Route::get('/listing/create', 'ListingController@create')->name('listing.create');
    Route::post('/listing/store', 'ListingController@store')->name('listing.store');
    Route::get('/listing/detail/{id}', 'ListingController@detail')->name('listing.detail');
    Route::get('/listing/edit/{id}', 'ListingController@edit')->name('listing.edit');
    Route::post('/listing/update/status', 'ListingController@updateStatus')->name('listing.update.status');
    Route::post('/listing/update/{id}', 'ListingController@update')->name('listing.update');
    Route::get('/listing/feature/add/{id}/{feature}/{value}', 'ListingController@addFeature');
    Route::post('/listing/image/upload/{id}', 'ListingController@uploadImage');
    Route::post('/listing/image/remove/{id}', 'ListingController@removeImage');

    Route::get('/house-order', 'OrderHouseController@index')->name('house-order');
    Route::get('/house-order/detail/{id}', 'OrderHouseController@detail')->name('house-order.detail');

    Route::get('/house-feature', 'HouseFeatureController@index')->name('house-feature');
    Route::get('/house-feature/create', 'HouseFeatureController@create')->name('house-feature.create');
    Route::post('/house-feature', 'HouseFeatureController@store')->name('house-feature.store');
    Route::get('/house-feature/detail/{id}', 'HouseFeatureController@detail')->name('house-feature.detail');
    Route::delete('/house-feature', 'HouseFeatureController@destroy')->name('house-feature.destroy');
});

Route::group(['prefix' => 'member', 'as' => 'member.', 'namespace' => 'Member', 'middleware' => ['role:member']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('/wishlist', 'HouseOrderController');
});

Route::group(['prefix' => 'surveyor', 'as' => 'surveyor.', 'namespace' => 'Surveyor', 'middleware' => ['role:surveyor']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
});

Route::group(['prefix' => 'mitra', 'as' => 'mitra.', 'namespace' => 'Mitra', 'middleware' => ['role:mitra']], function () {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('listing', 'ListingController');
    Route::get('/listing/feature/add/{id}/{feature}/{value}', 'ListingController@addFeature');
    Route::post('/listing/image/upload/{id}', 'ListingController@uploadImage');
    Route::post('/listing/image/remove/{id}', 'ListingController@removeImage');
    Route::get('/profile', 'ProfileController@index')->name('profile.index');
    Route::post('/profile', 'ProfileController@store')->name('profile.store');
});


Route::get('test', function(){
    return new \App\Mail\RegisterMail(\App\User::first());
});

Route::get('ttt', function(){
    $user = \App\User::find('U0343157');
    return view('profile-detail', compact('user'));
});